(function () {
    'use strict';
    angular.module('mainApp.error', ['ngRoute'])

            .config(['$routeProvider', function ($routeProvider) {
                    $routeProvider
                            .when('/unauthorized', {
                                templateUrl: 'app/components/error/unauthorized.html',
                                controller: 'ErrorController',
                                controllerAs: 'vm'
                            })
                            .when('/forbidden', {
                                templateUrl: 'app/components/error/forbidden.html',
                                controller: 'ErrorController',
                                controllerAs: 'vm'
                            })
                            .when('/unavailable', {
                                templateUrl: 'app/components/error/unavailable.html',
                                controller: 'ErrorController',
                                controllerAs: 'vm'
                            })
                            .when('/expired', {
                                templateUrl: 'app/components/error/expired.html',
                                controller: 'ErrorController',
                                controllerAs: 'vm'
                            })
                            .when('/error', {
                                templateUrl: 'app/components/error/error.html',
                                controller: 'ErrorController',
                                controllerAs: 'vm'
                            });
                }])
            .controller('ErrorController', ErrorController);

    ErrorController.$inject = ['$log', '$rootScope', '$location'];

    function ErrorController($log, $rootScope, $location) {
        
        var vm = this;
        vm.host = $location.host();        
        // vm.errorResponse = userCache.get("user-cache");
        // if (vm.errorResponse && vm.errorResponse.config && vm.errorResponse.config.data) {
        //     $log.info("user-cache :"+angular.toJson(vm.errorResponse));
        //     vm.errorResponse.config.data.pretty = angular.toJson(vm.errorResponse.config.data, true);
        // }
        // userCache.removeAll();
        // var userDetails = userInfoService.getUserDetails();
        // if (userDetails) {
        //     $rootScope.$broadcast('authorized');
        // } else {
        //     $rootScope.$broadcast('unauthorized');
        // }
        // vm.loginAction = function () {
        //     $location.path("/login").search({});
        // };
    }
}());

