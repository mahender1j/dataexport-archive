(function () {
    'use strict';
    angular.module('mainApp.services', [])
            .service('authenticationService', authenticationService);
    authenticationService.$inject = ['$http', '$q', '$log', '$window'];
    function authenticationService($http, $q, $log, $window) {
        var service = this;
        var userInfo = undefined;
        var userDetails = undefined;
       
        service.resetUserDetails = function () {
            userDetails = undefined;
        };

        service.getUserDetails = function () {
            return userDetails;
        };
        service.getUserLastLoggedInTS = function () {
            if ($window.sessionStorage["userInfo"]) {
                userInfo = JSON.parse($window.sessionStorage["userInfo"]);
                return userInfo.lastLoggedInTS;
            }
            return undefined;
        };
        service.setUserContactAcknowlege = function (userContactAcknowledge) {
            userDetails.userContactAcknowledge = userContactAcknowledge;
        };

        // service.login = function(loginForm){
        //     var defer = $q.defer();
        //     if(loginForm.emailAddress === 'mahender@g.com' && loginForm.password === 'password'){
        //         data = 'authenticated';
        //     }
        // }


        service.authenticateUser = function (loginInfo) {
            var defer = $q.defer();
            $http.post("api/users/login", loginInfo)
                    .success(function (data, status) {
                        $log.info("authenticationService.authenticateUser.response : " + angular.toJson(data));
                        switch (status) {
                            case 401 :
                                $log.warn('UserInfoService.authenticateUser : NOT AUTHENTICATED');
                                data = "authenticationFailed";
                                break;
                            case 423 :
                                $log.warn('UserInfoService.authenticateUser : ACCOUNT LOCKED');
                                data = "accountLocked";
                                break;
                            case 400 :
                                $log.warn('UserInfoService.authenticateUser : USER NOT FOUND');
                                data = "authenticationFailed";
                                break;
                            case 404 :
                                $log.warn('UserInfoService.authenticateUser : SERVICE IS NOT AVAILABLE');
                                data = "serviceUnavailable";
                                break;
                            case 200 :
                                userDetails = data;
                                break;
                            default :
                                data = "error";
                                $log.error('UserInfoService.authenticateUser : no data returned from login');
                                $window.sessionStorage.removeItem("userInfo");
                                service.resetUserDetails();
                                break;
                        };
                        defer.resolve(data);
                    })
                    .error(function (response) {
                        defer.reject(response);
                    });
            return defer.promise;
        };
        service.logout = function () {
            var deferred = $q.defer();
            if ($window.sessionStorage["userInfo"]) {
                userInfo = JSON.parse($window.sessionStorage["userInfo"]);
            } else {
                userInfo = undefined;
            }
            $log.warn(" UserInfoService.logout.userInfo : BEFORE LOGOUT : " + angular.toJson(userInfo));
            if (userInfo && userInfo.userId) {
                $http.defaults.headers.common.Authorization = 'Bearer ' + userInfo.accessToken;
                $http({
                    method: "POST",
                    url: "api/users/profile/logout/" + userInfo.userId
                }).success(function (result) {
                    $log.debug('UserInfoService.logout.result : ' + angular.toJson(result, true));
                    $window.sessionStorage.clear();  //clear all storage
                    userInfo = null;
                    $log.info("UserInfoService.logout.userInfo : AFTER LOGOUT : " + angular.toJson(userInfo));
                    $http.defaults.headers.common.Authorization = '';
                    service.resetUserDetails();
                    deferred.resolve(userDetails);
                }, function (error) {
                    deferred.reject(error);
                });
            } else {
                $http.defaults.headers.common.Authorization = '';
                $window.sessionStorage.clear();  //clear all storage
                userInfo = undefined;
                $log.info("UserInfoService.logout.userInfo : AFTER LOGOUT : " + angular.toJson(userInfo));
                service.resetUserDetails();
                deferred.resolve(userDetails);
            }
            return deferred.promise;
        };
        service.reloadUser = function () {
            var defer = $q.defer();
            if ($window.sessionStorage["userInfo"]) {
                userInfo = JSON.parse($window.sessionStorage["userInfo"]);
                $log.debug('UserInfoService.reloadUser.userInfo : ' + angular.toJson(userInfo));
                $window.sessionStorage.removeItem("userInfo");
                $http.defaults.headers.common.Authorization = 'Bearer ' + userInfo.accessToken;
                $log.info("userInfoService header token" + $http.defaults.headers.common.Authorization);
                $http.get("api/users/profile/details/" + userInfo.userId)
                        .success(function (response, status) {
                            switch (status) {
                                case 200 :
                                    userDetails = response;
                                    userInfo = {
                                        accessToken: response.access_token,
                                        userId: response.userId,
                                        lastLoggedInTS: userInfo.lastLoggedInTS
                                    };
                                    $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
                                    break;
                                default :
                                    userDetails = undefined;
                            };
                            $log.info("UserInfoService.reloadUser.userDetails : " + angular.toJson(userDetails, true));
                            defer.resolve(userDetails);
                        })
                        .error(function (response) {
                            defer.reject(response);
                        });

            } else {
                service.resetUserDetails();
                userInfo = undefined;
                defer.resolve(userDetails);
            }
            return defer.promise;
        };//end reloadUser
        
        return service;
    }
})();