(function () {
    'use strict';
    angular.module('mainApp.dataServices', [])
            .service('dataExtractService', dataExtractService);
    dataExtractService.$inject = ['$http', '$q', '$log', '$window'];


    function dataExtractService($http, $q, $log, $window) {
        var service = this;

        var itemList = undefined;
        
        var selectedWorkspaceId = undefined;
        
        service.getSelectedWorkspaceId = function() {
            return selectedWorkspaceId;
        }
        
        service.resetSelectedWorkspaceId = function(){
            selectedWorkspaceId = undefined;
        }

        service.getItemsList = function () {
            return itemList;
        }

        service.resetItemList = function () {
            itemList = undefined;
        }

        service.retrieveWorkspace = function (userDetails) {
            var defer = $q.defer();
            $log.info(angular.toJson(userDetails));
            $http.get("api/workspaces/"+ userDetails.tenantName+"/" + userDetails.customerToken + "?sessionId=" + userDetails.sessionid)
                    .success(function (response) {
                        defer.resolve(response);
                    })
                    .error(function (response) {
                        defer.reject(response);
                    })
            return defer.promise;
        };

        service.retrieveWorkspaceItems = function (userDetails, workspaceId) {
            var defer = $q.defer();
            selectedWorkspaceId = workspaceId;
            $http.get("api/workspaces/workspace/"+ userDetails.tenantName + "/" +userDetails.customerToken+"/" + workspaceId + "?sessionId=" + userDetails.sessionid)
                    .success(function (response) {
                        itemList = response;
                        defer.resolve(response);
                    })
                    .error(function (response) {
                        defer.reject(response);
                    })
            return defer.promise;
        }
        
        service.retrieveWorkspaceTabs = function(userDetails, selectedWorkspaceId, itemId){
            var defer = $q.defer();
            $http.get("api/workspace/tabs/"+ userDetails.tenantName+ "/" +userDetails.customerToken + "/" + selectedWorkspaceId +"/" + itemId + "?sessionId=" + userDetails.sessionid)
                    .success(function (response) {
                        $log.info(angular.toJson(response));
                        defer.resolve(response);
                    })
                    .error(function (response) {
                        defer.reject(response);
                    })
            return defer.promise;
        }
        
        service.generateExtract = function(userDetails, selectedWorkspaceId, selectedSchema, extracttype,
                                        selectedUser,startDate, endDate, recordCount){
        var defer = $q.defer();
            $http.post("api/workspace/extractdata/"+ userDetails.tenantName+ "/" +userDetails.customerToken + "/" + selectedWorkspaceId +"?sessionId=" + userDetails.sessionid +
                        "&extracttype="+extracttype +"&selectedUser="+selectedUser +"&startDate="+ startDate +"&endDate="+ endDate + "&recordCount="+recordCount, selectedSchema)
                    .success(function (response) {
                        $log.info(angular.toJson(response));
                        defer.resolve(response);
                    })
                    .error(function (response) {
                        defer.reject(response);
                    })
            return defer.promise;
        }
        
        service.retreiveUserList = function(userDetails){
            var defer = $q.defer();
            var url = "api/users/userList/"+ userDetails.tenantName+ "/" +userDetails.customerToken +"?sessionId=" + userDetails.sessionid;
            $log.info(url);
            $http.get(url)
                    .success(function (response){
                        $log.info(angular.toJson(response));
                        defer.resolve(response);
                    })
                    .error(function (response){
                        defer.reject(response);
                    })
                    
           return defer.promise;
        }
        
        service.retriveSchema = function(schemaName, userDetails, selectedWorkspaceId, dmsId){
            var defer = $q.defer();
            schemaName = schemaName.replace(/\s/g, '');
            $http.get("api/workspace/"+schemaName+"/"+ userDetails.tenantName+ "/" +userDetails.customerToken + "/" + selectedWorkspaceId +"/" + dmsId + "?sessionId=" + userDetails.sessionid)
                    .success(function (response) {
                        $log.info(angular.toJson(response));
                        defer.resolve(response);
                    })
                    .error(function (response) {
                        defer.reject(response);
                    })
            return defer.promise;
        }  
        
        return service;
    }
})();