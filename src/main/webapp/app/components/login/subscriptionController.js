'use strict';
(function() {
    angular.module('mainApp.subscription', ['ngRoute'])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/subscription', {
                templateUrl: 'app/components/login/subscription.html',
                controller: 'subscriptionController',
                controllerAs: 'vm'
            });
        }])
        .controller('subscriptionController', subscriptionController)
    subscriptionController.$inject = ['$log', '$location','authenticationService', '$rootScope'];
    function subscriptionController( $log, $location,authenticationService, $rootScope){
        var vm = this;
        vm.messageCount = 0;
        vm.success = false;
        vm.message = "";
        
        vm.loginForm = {
            userID : "mahender@hummingbirdinnovations.com",
            password : "hummingbird123",
            tenantName: "previewhummingbirdinnovations"
        }
        
        vm.loginAction = function(){
            authenticateUser();
        };
        
        function authenticateUser(){
            vm.message = "";
            authenticationService.authenticateUser(vm.loginForm)
                .then(function (data) {
                    if(data.authStatus.description === "LOGIN_OK"){
                        $log.info("successfully authenticated"+angular.toJson(data));
                        $rootScope.$broadcast('authentication-success');
                        $location.path("/home");
                    } else if(data.authStatus.description === 'LOGIN_FAILED'){
                        vm.message = 'Verfication Failed. Please check your credentials.'
                        $rootScope.$broadcast('authentication-failed');
                    }
             });
        }
    }
}());