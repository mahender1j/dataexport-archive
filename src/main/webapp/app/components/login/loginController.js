(function () {
    'use strict';
    angular.module('mainApp.login', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
                .when('/login', {
                    templateUrl:'app/components/login/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm'
                });
        }])
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$log', '$location', '$rootScope'];

    function LoginController($log, $location, $rootScope) {
        var vm = this;
        vm.message = "";
        vm.loginForm = {
            "emailAddress" : undefined,
            "password" : undefined,
            "accessKey" : undefined
        }

        vm.loginAction = function(){
            vm.message = "";
            authenticateUser();
        };

        function authenticateUser(){
            if(vm.loginForm.emailAddress === '1' && vm.loginForm.password === '1'
                    && vm.loginForm.accessKey === '1'){
                $log.info('authenticated');
                $location.path("/subscription");
            } else {
                vm.message = "Please check your credentials.";
               
                $log.info("failed");
            }
            // authenticationService.authenticateUser(vm.loginForm)
            //     .then(function (data) {
            //         if(data.authStatus.description === "LOGIN_OK"){
            //             $log.info("successfully authenticated");
            //             $location.path("/home");
            //         }
            //     });
        }


    };

}());