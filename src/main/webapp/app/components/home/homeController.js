'use strict';
(function () {
    angular.module('mainApp.home', ['ngRoute'])

            .config(['$routeProvider', function ($routeProvider) {
                    $routeProvider.when('/home', {
                        templateUrl: 'app/components/home/home.html',
                        controller: 'HomeController',
                        controllerAs: 'vm'
                    });
                }])
            .controller('HomeController', HomeController)

    HomeController.$inject = ['$log', 'authenticationService', 'dataExtractService', '$location'];

    function HomeController($log, authenticationService, dataExtractService, $location) {
        var vm = this;
        vm.username = "";
        vm.wfResponse = undefined;
        vm.selectedWorkspaceId = undefined;
        vm.itemList = [];
        vm.newdata = "active";
        vm.title = 'Data Extraction - New';
        vm.numberOfPages = 0;

        vm.init = function () {
            dataExtractService.retrieveWorkspace(authenticationService.getUserDetails())
                    .then(function (data) {
                        $log.info(data);
                        vm.wfResponse = data;

                    })
        }

        vm.sumbitAction = function () {
            $log.info("selected workspaceId" + angular.toJson(vm.selectedWorkspaceId));


            dataExtractService.retrieveWorkspaceItems(authenticationService.getUserDetails(), vm.selectedWorkspaceId)
                    .then(function (data) {
                        $log.info(data);
                        vm.itemList = data;
                        $log.info(angular.toJson(vm.itemList));
                        //$location.path("/item");
                        
                         vm.numberOfPages = Math.ceil(vm.itemList.elements.length / vm.pageSize);
                        
        
                    })
        }

        vm.itemClicked = function (menuItem, title) {
            $log.info("index value" + menuItem);
            vm.title = title;
            vm.newdata = "";
            vm.existingdata = "";
            vm.newupdate = "";
            vm.existingupdate = "";
            vm.new2Record = "";
            vm.existingNewRecord = "";

            if (menuItem === 'newdata') {
                vm.newdata = "active";
            } else if (menuItem === 'existingdata') {
                vm.existingdata = "active";
            } else if (menuItem === 'newupdate') {
                vm.newupdate = "active";
            } else if (menuItem === 'existingupdate') {
                vm.existingupdate = "active";
            } else if (menuItem === 'new2Record') {
                vm.new2Record = "active";
            } else if (menuItem === 'existingNewRecord') {
                vm.existingNewRecord = "active";
            }
        }

        vm.curPage = 0;
        vm.pageSize = 25;

        
        
        

    }
}());

