'use strict';
(function () {
    angular.module('mainApp.itemDetails', ['ngRoute'])

            .config(['$routeProvider', function ($routeProvider) {
                    $routeProvider.when('/itemDetails', {
                        templateUrl: 'app/components/item/itemDetails.html',
                        controller: 'ItemDetailsController',
                        controllerAs: 'vm'
                    });
                }])
            .controller('ItemDetailsController', ItemDetailsController)

    ItemDetailsController.$inject = ['$log', 'authenticationService', 'dataExtractService', '$location'];

    function ItemDetailsController($log, authenticationService, dataExtractService, $location) {
        var vm = this;
        vm.item = {
            name: undefined,
            value: false
        }
        vm.itemList = undefined;
        vm.userList = undefined;
        vm.itemSubList = [];
        vm.message = undefined;
        vm.exportSelectedItems = [];
        vm.selectedSchema = {
            selectedItems: []
        }
//        vm.selectedItem = {
//            itemName : "",
//            itemList : []
//        }
        vm.selectedItems = [];
        vm.extracttype = "excel";
        vm.selectedUser = "";
        vm.startDate = "";
        vm.endDate = "";
        vm.recordCount = "";

        vm.init = function () {
            vm.selectedWorkspaceId = dataExtractService.getSelectedWorkspaceId();
            $log.info("selected WorkspaceId" + vm.selectedWorkspaceId);
            vm.itemId = $location.search().itemId;
            
            $log.info("item id" + vm.itemId);

            vm.userDetails = authenticationService.getUserDetails();
            dataExtractService.retrieveWorkspaceTabs(vm.userDetails, vm.selectedWorkspaceId, vm.itemId)
                    .then(function (data) {
                        $log.info(data);
                        vm.itemList = data;
                    })
                    
            dataExtractService.retreiveUserList(vm.userDetails)
                    .then(function (data){
                        $log.info(data);
                        vm.userList = data;
                        $log.info(vm.userList);
            })
        }

        vm.toggleDetail = function ($index) {
            //$scope.isVisible = $scope.isVisible == 0 ? true : false;
            vm.activePosition = vm.activePosition === $index ? -1 : $index;
        };

        vm.getItemDetails = function (item) {
            vm.itemName = item.name;
            var schemaName = "";
//            if(item.key === 'Project Management'){
//                schemaName = item.key;
//            } else {
//                schemaName = item.name;
//            }
            schemaName = item.key;
            vm.itemSubList = [];
            dataExtractService.retriveSchema(schemaName, authenticationService.getUserDetails(), vm.selectedWorkspaceId, vm.itemId)
                    .then(function (data) {
                        $log.info(data);
                        vm.verifyItemDetails(data, item.name);
            })
        }

        vm.verifyItemDetails = function (data, name) {
            angular.forEach(data, function (i) {
                vm.item.name = i;
                vm.item.value = false;
                var matched = false;
                $log.info(angular.toJson(vm.item));

                if (vm.selectedSchema.selectedItems.length > 0) {
                    angular.forEach(vm.selectedSchema.selectedItems, function (it) {
                        if (it.itemName === name) {
                            angular.forEach(it.itemList, function (itm) {
                                if (itm.name === i) {
                                    vm.itemSubList.push({'name': i, 'value': true});
                                    matched = true;
                                }
                            })
                        }
                    })
                }
                if (vm.selectedSchema.selectedItems.length < 1 || matched === false) {
                    vm.itemSubList.push({'name': i, 'value': false});
                }

            })
        }

        vm.drag = function (i) {
            if (i) {
                vm.item.name = i.name;
                vm.item.value = false;
                if (i.value === true) {
                    //vm.exportSelectedItems.push({'name':i.name, 'value':false});
                    if (vm.selectedSchema.selectedItems.length > 0) {
                        var matched = false;
                        angular.forEach(vm.selectedSchema.selectedItems, function (itemNem) {
                            if (itemNem.itemName === vm.itemName) {
                                $log.info("record already added");
                                itemNem.itemList.push({'name': i.name, 'value': false});
                                matched = true;
                            }
                        })
                        if (matched === false) {
                            var list = [];
                            list.push({'name': i.name, 'value': false});
                            vm.selectedSchema.selectedItems.push({'itemName': vm.itemName, 'itemList': list});
                        }
                    } else {
                        var list = [];
                        list.push({'name': i.name, 'value': false});
                        vm.selectedSchema.selectedItems.push({'itemName': vm.itemName, 'itemList': list});
                    }


                }
            }
        }

        vm.drop = function (index, itemName, i) {

            var ind = 0;
            var matched = false;
            if (vm.selectedSchema.selectedItems.length > 0) {
                angular.forEach(vm.selectedSchema.selectedItems, function (it) {
                    if (it.itemName === itemName) {
                        it.itemList.splice(index, 1);
                        if (it.itemList.length < 1) {
                            ind = ind + 1;
                            matched = true;
                        } else {
                            ind = ind + 1;
                        }
                    }
                })
                if (matched === true) {
                    vm.selectedSchema.selectedItems.splice(ind - 1, 1);
                }

                angular.forEach(vm.itemSubList, function (itm) {
                    if (itm.name === i.name) {
                        itm.value = false;
                    }
                })
            }

        }

        vm.deleteSeletedItemGroup = function (index) {
            if (vm.selectedSchema.selectedItems.length > 0) {
                if (vm.selectedSchema.selectedItems[index].itemName === vm.itemName) {
                    angular.forEach(vm.itemSubList, function (itm) {
                        itm.value = false;
                    })
                }
                vm.selectedSchema.selectedItems.splice(index, 1);
            }
        }

        vm.generateExtract = function () {
            $log.info("WorkspaceId: " + vm.selectedWorkspaceId);
            dataExtractService.generateExtract(vm.userDetails, vm.selectedWorkspaceId, vm.selectedSchema, vm.extracttype, vm.selectedUser,
                    vm.startDate, vm.endDate, vm.recordCount)
                    .then(function (data) {
                        $log.info(data);
                        $log.info("Call success");

                    })
        }




    }
}());

