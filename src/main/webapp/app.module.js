/**
 * @author tgjah
 *
 * This file contains the mainApp module.
 * Note: do not change the name of this variable as it links all the controllers to the mainApp module within the html. 
 */
(function () {
    'use strict';
    angular.module('mainApp', [
        'ngRoute', 'mainApp.services', 'mainApp.dataServices', 'mainApp.tablePagination', 'mainApp.login',
        'mainApp.error', 'mainApp.subscription', 'mainApp.home', 'mainApp.itemDetails', '720kb.datepicker'
    ])
            .config(['$routeProvider', function ($routeProvider) {
                    $routeProvider
                            .when('/error', {
                                templateUrl: 'app/components/error/error.html',
                                controller: 'ErrorController',
                                controllerAs: 'vm'
                            })
                            .otherwise({redirectTo: '/login'});
                }])
            .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$rootScope', '$timeout', '$location', '$log', 'authenticationService'];

    function MainCtrl($rootScope, $timeout, $location, $log, authenticationService) {
        var main = this;

        $rootScope.$on('authentication-success', function () {
            main.currentUser = authenticationService.getUserDetails();
            $log.info("MainCtrl.authorized.currentUser : " + angular.toJson(main.currentUser, true));
        });
    }
}());