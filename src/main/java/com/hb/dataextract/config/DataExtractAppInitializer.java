package com.hb.dataextract.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class DataExtractAppInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext servletContext) {
		WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        //servletContext.addFilter("springSecurityFilterChain", "org.springframework.web.filter.DelegatingFilterProxy").addMappingForUrlPatterns(null, false, "/api/*");
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/api/*");
        
       
        
    }

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation("com.hb.dataextract");
        return context;
    }
    
}