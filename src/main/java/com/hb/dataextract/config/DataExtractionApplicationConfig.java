/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.config;

/**
 *
 * @author ramimmidisetti
 */
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy(proxyTargetClass=true) 
@ComponentScan({"com.hb.dataextract"}) 
@Import(DatabaseConfig.class)
@PropertySource(value = "classpath:application.properties")
public class DataExtractionApplicationConfig {
    
}
