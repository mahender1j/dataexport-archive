/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.config;

import com.hb.dataextract.batch.ResponseReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Mahe
 */
//@Configuration
public class BatchConfig {
   @Bean
   public  ResponseReader restStudentReader(Integer workspaceId, String restUrl) {
        return new ResponseReader(restUrl, workspaceId);
    }
}
