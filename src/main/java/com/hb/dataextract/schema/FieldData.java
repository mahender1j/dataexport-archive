/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Mahe
 */
public class FieldData {
    @JsonProperty("value")
    private String value;
    @JsonProperty("formattedValue")
    private String formattedValue;
    @JsonProperty("dataType")
    private String dataType;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFormattedValue() {
        return formattedValue;
    }

    public void setFormattedValue(String formattedValue) {
        this.formattedValue = formattedValue;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
    
    
            
}
