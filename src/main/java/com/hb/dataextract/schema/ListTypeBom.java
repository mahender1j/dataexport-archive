/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema;

import com.hb.dataextract.schema.bom.BomItem;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Mahe
 */
public class ListTypeBom {
    @XmlElement(name = "data")
    private BomItem[] data;

    public BomItem[] getData() {
        return data;
    }

    public void setData(BomItem[] data) {
        this.data = data;
    }
    
    
}
