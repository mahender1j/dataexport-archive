/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.projectitem;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 *
 * @author Mahe
 */
@XmlRootElement
public class ProjectItem {
    private Integer id;
    private Integer dmsID;
    private String descriptor;
    private Date startDate;
    private Date endDate;
    private Integer progress;
    private String projectItemType;
    private String status;
    private String duration;
    private ProjectItem[] projectItem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDmsID() {
        return dmsID;
    }

    public void setDmsID(Integer dmsID) {
        this.dmsID = dmsID;
    }

    
    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getProjectItemType() {
        return projectItemType;
    }

    public void setProjectItemType(String projectItemType) {
        this.projectItemType = projectItemType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public ProjectItem[] getProjectItem() {
        return projectItem;
    }

    public void setProjectItem(ProjectItem[] projectItem) {
        this.projectItem = projectItem;
    }
    
    
    
    
}
