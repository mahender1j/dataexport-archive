/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.projectitem;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement
public class Project {
    private Integer dmsID;
    private Integer progress;
    private Date startDate;
    private Date endDate;
    
    private ProjectItem projectItems;

    public Integer getDmsID() {
        return dmsID;
    }

    public void setDmsID(Integer dmsID) {
        this.dmsID = dmsID;
    }

    

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ProjectItem getProjectItems() {
        return projectItems;
    }

    public void setProjectItems(ProjectItem projectItems) {
        this.projectItems = projectItems;
    }

   
    
    
}
