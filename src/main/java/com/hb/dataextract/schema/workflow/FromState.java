/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.workflow;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement
public class FromState {
    
     private Integer stateID;
    private Boolean cmStateID;
    private Boolean lockStateID;
    private Integer coordinateX;
    private Integer coordinateY;
    private Integer width;
    private Integer height;
    private Boolean hidden;

    public Integer getStateID() {
        return stateID;
    }

    public void setStateID(Integer stateID) {
        this.stateID = stateID;
    }

    public Boolean getCmStateID() {
        return cmStateID;
    }

    public void setCmStateID(Boolean cmStateID) {
        this.cmStateID = cmStateID;
    }

    public Boolean getLockStateID() {
        return lockStateID;
    }

    public void setLockStateID(Boolean lockStateID) {
        this.lockStateID = lockStateID;
    }

    public Integer getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(Integer coordinateX) {
        this.coordinateX = coordinateX;
    }

    public Integer getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(Integer coordinateY) {
        this.coordinateY = coordinateY;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }


    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
   
    
    
    
}
