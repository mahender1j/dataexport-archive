/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.workflow;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement
public class Transition {
    private Integer transitionID;
    private String shortName;
    private String longName;
    private Integer permissionID;
    private Integer escalationPermissionId;
    private String customID;
    private Double labelPositionA;
    private Double labelPositionB;
    private Boolean hidden;
    private Boolean email;
    private Boolean outstanding;
    private Boolean notifyPerformers;
    private Boolean passwordApproval;
    private Integer commentsForApproval;
    private Integer scriptConditionId;
    private Integer scriptValidationId;
    private Integer scriptActionId;
    
    private FromState fromState;
    private ToState toState;
    private PointsList[] pointsList;

    public Integer getTransitionID() {
        return transitionID;
    }

    public void setTransitionID(Integer transitionID) {
        this.transitionID = transitionID;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public Integer getPermissionID() {
        return permissionID;
    }

    public void setPermissionID(Integer permissionID) {
        this.permissionID = permissionID;
    }

    public Integer getEscalationPermissionId() {
        return escalationPermissionId;
    }

    public void setEscalationPermissionId(Integer escalationPermissionId) {
        this.escalationPermissionId = escalationPermissionId;
    }

    public String getCustomID() {
        return customID;
    }

    public void setCustomID(String customID) {
        this.customID = customID;
    }

    public Double getLabelPositionA() {
        return labelPositionA;
    }

    public void setLabelPositionA(Double labelPositionA) {
        this.labelPositionA = labelPositionA;
    }

    public Double getLabelPositionB() {
        return labelPositionB;
    }

    public void setLabelPositionB(Double labelPositionB) {
        this.labelPositionB = labelPositionB;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getEmail() {
        return email;
    }

    public void setEmail(Boolean email) {
        this.email = email;
    }

    public Boolean getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(Boolean outstanding) {
        this.outstanding = outstanding;
    }

    public Boolean getNotifyPerformers() {
        return notifyPerformers;
    }

    public void setNotifyPerformers(Boolean notifyPerformers) {
        this.notifyPerformers = notifyPerformers;
    }

    public Boolean getPasswordApproval() {
        return passwordApproval;
    }

    public void setPasswordApproval(Boolean passwordApproval) {
        this.passwordApproval = passwordApproval;
    }

    public Integer getCommentsForApproval() {
        return commentsForApproval;
    }

    public void setCommentsForApproval(Integer commentsForApproval) {
        this.commentsForApproval = commentsForApproval;
    }

    public Integer getScriptConditionId() {
        return scriptConditionId;
    }

    public void setScriptConditionId(Integer scriptConditionId) {
        this.scriptConditionId = scriptConditionId;
    }

    public Integer getScriptValidationId() {
        return scriptValidationId;
    }

    public void setScriptValidationId(Integer scriptValidationId) {
        this.scriptValidationId = scriptValidationId;
    }

    public Integer getScriptActionId() {
        return scriptActionId;
    }

    public void setScriptActionId(Integer scriptActionId) {
        this.scriptActionId = scriptActionId;
    }

    public FromState getFromState() {
        return fromState;
    }

    public void setFromState(FromState fromState) {
        this.fromState = fromState;
    }

    public ToState getToState() {
        return toState;
    }

    public void setToState(ToState toState) {
        this.toState = toState;
    }

    public PointsList[] getPointsList() {
        return pointsList;
    }

    public void setPointsList(PointsList[] pointsList) {
        this.pointsList = pointsList;
    }
    
    
    
}
