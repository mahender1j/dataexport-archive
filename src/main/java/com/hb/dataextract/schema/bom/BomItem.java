/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.bom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hb.dataextract.schema.Fields;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement(name = "bom-item")
public class BomItem {
    private Integer dmsID;
    private Integer workspaceType;
    private Integer workspaceID;
    private Integer bomDepthLevel;
    private Integer quantity;
    private String formattedQuantity;
    private Integer itemNumber;
    private Integer cost;
    private Integer quoteID;
    private Integer redlinedCost;
    private Integer redlineAgainstVersion;
    private Integer totalWeight;
    
    @JsonProperty("bom-item")
    private BomItem bomItem;

    private boolean assembly;
    private boolean isPinned;
    private boolean isUsingDefaultQuote;
    private boolean leaf;
    private boolean redlineAddition;
    private boolean redlineDeletion;
    private boolean hasSourcing;
    private boolean hasRedlinedSourcing;

    private String descriptor;
    private String revision;
    private String units;
    private String lifecycleStatus;
    
    @JsonProperty("fields")
    private Fields fields;

    public Integer getDmsID() {
        return dmsID;
    }

    public void setDmsID(Integer dmsID) {
        this.dmsID = dmsID;
    }

    public Integer getWorkspaceType() {
        return workspaceType;
    }

    public void setWorkspaceType(Integer workspaceType) {
        this.workspaceType = workspaceType;
    }

    public Integer getWorkspaceID() {
        return workspaceID;
    }

    public void setWorkspaceID(Integer workspaceID) {
        this.workspaceID = workspaceID;
    }

    public Integer getBomDepthLevel() {
        return bomDepthLevel;
    }

    public void setBomDepthLevel(Integer bomDepthLevel) {
        this.bomDepthLevel = bomDepthLevel;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getFormattedQuantity() {
        return formattedQuantity;
    }

    public void setFormattedQuantity(String formattedQuantity) {
        this.formattedQuantity = formattedQuantity;
    }

    public Integer getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(Integer itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getQuoteID() {
        return quoteID;
    }

    public void setQuoteID(Integer quoteID) {
        this.quoteID = quoteID;
    }

    public Integer getRedlinedCost() {
        return redlinedCost;
    }

    public void setRedlinedCost(Integer redlinedCost) {
        this.redlinedCost = redlinedCost;
    }

    public Integer getRedlineAgainstVersion() {
        return redlineAgainstVersion;
    }

    public void setRedlineAgainstVersion(Integer redlineAgainstVersion) {
        this.redlineAgainstVersion = redlineAgainstVersion;
    }

    public Integer getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Integer totalWeight) {
        this.totalWeight = totalWeight;
    }

    public boolean isAssembly() {
        return assembly;
    }

    public void setAssembly(boolean assembly) {
        this.assembly = assembly;
    }

    public boolean isIsPinned() {
        return isPinned;
    }

    public void setIsPinned(boolean isPinned) {
        this.isPinned = isPinned;
    }

    public boolean isIsUsingDefaultQuote() {
        return isUsingDefaultQuote;
    }

    public void setIsUsingDefaultQuote(boolean isUsingDefaultQuote) {
        this.isUsingDefaultQuote = isUsingDefaultQuote;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public boolean isRedlineAddition() {
        return redlineAddition;
    }

    public void setRedlineAddition(boolean redlineAddition) {
        this.redlineAddition = redlineAddition;
    }

    public boolean isRedlineDeletion() {
        return redlineDeletion;
    }

    public void setRedlineDeletion(boolean redlineDeletion) {
        this.redlineDeletion = redlineDeletion;
    }

    public boolean isHasSourcing() {
        return hasSourcing;
    }

public void setHasSourcing(boolean hasSourcing) {
        this.hasSourcing = hasSourcing;
    }

    public boolean isHasRedlinedSourcing() {
        return hasRedlinedSourcing;
    }

    public void setHasRedlinedSourcing(boolean hasRedlinedSourcing) {
        this.hasRedlinedSourcing = hasRedlinedSourcing;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getLifecycleStatus() {
        return lifecycleStatus;
    }

    public void setLifecycleStatus(String lifecycleStatus) {
        this.lifecycleStatus = lifecycleStatus;
    }

    public BomItem getBomItem() {
        return bomItem;
    }

    public void setBomItem(BomItem bomItem) {
        this.bomItem = bomItem;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }
    
    
}
