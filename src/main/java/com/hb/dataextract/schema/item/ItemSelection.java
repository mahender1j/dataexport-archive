/**
 * 
 */
package com.hb.dataextract.schema.item;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class ItemSelection {
	private List<ItemSelection> list;
	private String uri;
	private String description;
	private Integer id;
	
	private Detail details;
	private Map<String, ItemSelection[]> relations;
	private Map<String, FieldData[]> metaFields;
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Detail getDetails() {
		return details;
	}
	public void setDetails(Detail details) {
		this.details = details;
	}
	public Map<String, ItemSelection[]> getRelations() {
		return relations;
	}
	public void setRelations(Map<String, ItemSelection[]> relations) {
		this.relations = relations;
	}
	public Map<String, FieldData[]> getMetaFields() {
		return metaFields;
	}
	public void setMetaFields(Map<String, FieldData[]> metaFields) {
		this.metaFields = metaFields;
	}
	public List<ItemSelection> getList() {
		return list;
	}
	public void setList(List<ItemSelection> list) {
		this.list = list;
	}

}
