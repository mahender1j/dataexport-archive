/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.item.v2;

import com.hb.dataextract.schema.item.AdditionalOwner;
import com.hb.dataextract.schema.item.CreatedByUser;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */

@XmlRootElement
public class Audit {
    private String lastModiflastModified;
    private String createdOn;
    
    private CreatedByUser createdBy;
    private AdditionalOwner[] additionalUserOwners;

   

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModiflastModified() {
        return lastModiflastModified;
    }

    public void setLastModiflastModified(String lastModiflastModified) {
        this.lastModiflastModified = lastModiflastModified;
    }

    public CreatedByUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedByUser createdBy) {
        this.createdBy = createdBy;
    }

    public AdditionalOwner[] getAdditionalUserOwners() {
        return additionalUserOwners;
    }

    public void setAdditionalUserOwners(AdditionalOwner[] additionalUserOwners) {
        this.additionalUserOwners = additionalUserOwners;
    }
    
    
    
}
