/**
 * 
 */
package com.hb.dataextract.schema.item;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class AdditionalOwner implements Serializable{

	private static final long serialVersionUID = 2971082487927636416L;
	private String group;
	private String user;
	
	
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	
	
}
