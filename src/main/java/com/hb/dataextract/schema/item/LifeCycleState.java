/**
 * 
 */
package com.hb.dataextract.schema.item;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class LifeCycleState {
	private Integer stateID;
	private String stateName;
	private boolean effectivity;
	
	
	public Integer getStateID() {
		return stateID;
	}
	public void setStateID(Integer stateID) {
		this.stateID = stateID;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public boolean isEffectivity() {
		return effectivity;
	}
	public void setEffectivity(boolean effectivity) {
		this.effectivity = effectivity;
	}
	
	
}
