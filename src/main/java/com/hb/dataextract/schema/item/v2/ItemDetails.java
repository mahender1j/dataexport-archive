/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.item.v2;

import com.hb.dataextract.schema.item.AdditionalOwner;
import com.hb.dataextract.schema.item.CreatedByUser;
import com.hb.dataextract.schema.item.LifeCycleState;
import com.hb.dataextract.schema.item.Owner;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement
public class ItemDetails {
    
    private String deleted;
    private String itemDescriptor;
    private String fullItemDescriptor;
    private String revision;
    private Integer id;
    private Integer rootId;
    private Integer version;
    private Integer workspaceId;
    private boolean isLatestVersion;
    private boolean isWorkingVersion;

    private Ownership ownership;
    private Audit audit;

    public Ownership getOwnership() {
        return ownership;
    }

    public void setOwnership(Ownership ownership) {
        this.ownership = ownership;
    }

    public Audit getAudit() {
        return audit;
    }

    public void setAudit(Audit audit) {
        this.audit = audit;
    }
    
    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getItemDescriptor() {
        return itemDescriptor;
    }

    public void setItemDescriptor(String itemDescriptor) {
        this.itemDescriptor = itemDescriptor;
    }

    public String getFullItemDescriptor() {
        return fullItemDescriptor;
    }

    public void setFullItemDescriptor(String fullItemDescriptor) {
        this.fullItemDescriptor = fullItemDescriptor;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRootId() {
        return rootId;
    }

    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(Integer workspaceId) {
        this.workspaceId = workspaceId;
    }

    public boolean isIsLatestVersion() {
        return isLatestVersion;
    }

    public void setIsLatestVersion(boolean isLatestVersion) {
        this.isLatestVersion = isLatestVersion;
    }

    public boolean isIsWorkingVersion() {
        return isWorkingVersion;
    }

    public void setIsWorkingVersion(boolean isWorkingVersion) {
        this.isWorkingVersion = isWorkingVersion;
    }

   
       
}
