/**
 * 
 */
package com.hb.dataextract.schema.item;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class FieldData implements Serializable {

	private static final long serialVersionUID = 6301044377391680956L;
	private String value;
	private String formattedValue;
	private String dataType;
	
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getFormattedValue() {
		return formattedValue;
	}
	public void setFormattedValue(String formattedValue) {
		this.formattedValue = formattedValue;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	
	
}
