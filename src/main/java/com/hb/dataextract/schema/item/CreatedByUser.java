/**
 *
 */
package com.hb.dataextract.schema.item;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class CreatedByUser implements Serializable {

    private static final long serialVersionUID = -7050172173513291080L;

    private String displayName;
    private String id;
    private String loginName;
    private String organization;
    private String licenseType;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

}
