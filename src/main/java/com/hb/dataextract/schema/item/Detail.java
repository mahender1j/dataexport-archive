/**
 * 
 */
package com.hb.dataextract.schema.item;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class Detail implements Serializable{
	private static final long serialVersionUID = 4406347887775044981L;
	
	private String deleted;
	private String descriptor;
	private String release;
	private Integer dmsID;
	private Integer versionID;
	private Integer workspaceID;
	private Integer version;
	private Date timeStamp;
	private String lifecycleStatus;
	private boolean latest;
	private boolean working;
	private String status;
	
	private CreatedByUser createdByUser;
	private Owner owner;
	private AdditionalOwner additionalOwners;
	private LifeCycleState lifeCycleState;
	
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	public String getRelease() {
		return release;
	}
	public void setRelease(String release) {
		this.release = release;
	}
	public Integer getDmsID() {
		return dmsID;
	}
	public void setDmsID(Integer dmsID) {
		this.dmsID = dmsID;
	}
	public Integer getVersionID() {
		return versionID;
	}
	public void setVersionID(Integer versionID) {
		this.versionID = versionID;
	}
	public Integer getWorkspaceID() {
		return workspaceID;
	}
	public void setWorkspaceID(Integer workspaceID) {
		this.workspaceID = workspaceID;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getLifecycleStatus() {
		return lifecycleStatus;
	}
	public void setLifecycleStatus(String lifecycleStatus) {
		this.lifecycleStatus = lifecycleStatus;
	}
	public boolean isLatest() {
		return latest;
	}
	public void setLatest(boolean latest) {
		this.latest = latest;
	}
	public boolean isWorking() {
		return working;
	}
	public void setWorking(boolean working) {
		this.working = working;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public CreatedByUser getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(CreatedByUser createdByUser) {
		this.createdByUser = createdByUser;
	}
	public AdditionalOwner getAdditionalOwners() {
		return additionalOwners;
	}
	public void setAdditionalOwners(AdditionalOwner additionalOwners) {
		this.additionalOwners = additionalOwners;
	}
	public LifeCycleState getLifeCycleState() {
		return lifeCycleState;
	}
	public void setLifeCycleState(LifeCycleState lifeCycleState) {
		this.lifeCycleState = lifeCycleState;
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	
	
	

}
