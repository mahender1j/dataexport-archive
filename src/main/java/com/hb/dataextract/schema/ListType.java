/**
 * 
 */
package com.hb.dataextract.schema;

import javax.xml.bind.annotation.XmlRootElement;

import com.hb.dataextract.schema.item.ItemSelection;
import com.hb.dataextract.schema.attachment.ItemAttachment;
import com.hb.dataextract.schema.workflow.Transition;
import com.hb.dataextract.schema.Data;
import com.hb.dataextract.schema.bom.BomItem;
import com.hb.dataextract.schema.user.User;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class ListType {
        
	private ItemSelection[] item;
        @XmlElement(name = "data")
        private ItemAttachment[] data; 
        private Transition[] transition;
        private User[] user;

        
    
	
	
	public ItemSelection[] getItem() {
		return item;
	}
	public void setItem(ItemSelection[] item) {
		this.item = item;
	}

    public Transition[] getTransition() {
        return transition;
    }

    public ItemAttachment[] getData() {
        return data;
    }

    public void setData(ItemAttachment[] data) {
        this.data = data;
    }

    public void setTransition(Transition[] transition) {
        this.transition = transition;
    }

    public User[] getUser() {
        return user;
    }

    public void setUser(User[] user) {
        this.user = user;
    }
	
	
}
