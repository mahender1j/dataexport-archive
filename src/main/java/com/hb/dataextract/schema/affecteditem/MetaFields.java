/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.affecteditem;

import com.hb.dataextract.schema.item.FieldData;

/**
 *
 * @author Mahe
 */
public class MetaFields {
    private String key;
    private FieldData fieldData;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public FieldData getFieldData() {
        return fieldData;
    }

    public void setFieldData(FieldData fieldData) {
        this.fieldData = fieldData;
    }
    
    
    
}
