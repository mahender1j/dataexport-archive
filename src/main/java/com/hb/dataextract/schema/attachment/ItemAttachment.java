/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema.attachment;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement
public class ItemAttachment {
    private String uri;
    private ItemAttachment file;
     private boolean deleted;
    private Integer versionID;
    private Integer dmsID;
    private Integer workspaceID;
    private Long fileSize; 
    private Integer fileID;
    private String fileName;
    private String resourceName;    
    private String description;
    private Integer fileVersion;
    private Integer statusID;
    private FileStatus fileStatus;
    private Integer folderId;
    private Date timestamp;
    private String createdUserID;
    private String createdDisplayName;
    private String fileStorageKeyHash;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getVersionID() {
        return versionID;
    }

    public void setVersionID(Integer versionID) {
        this.versionID = versionID;
    }

    public Integer getDmsID() {
        return dmsID;
    }

    public void setDmsID(Integer dmsID) {
        this.dmsID = dmsID;
    }

    public Integer getWorkspaceID() {
        return workspaceID;
    }

    public void setWorkspaceID(Integer workspaceID) {
        this.workspaceID = workspaceID;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Integer getFileID() {
        return fileID;
    }

    public void setFileID(Integer fileID) {
        this.fileID = fileID;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getFileStorageKeyHash() {
        return fileStorageKeyHash;
    }

    public void setFileStorageKeyHash(String fileStorageKeyHash) {
        this.fileStorageKeyHash = fileStorageKeyHash;
    }

   

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFileVersion() {
        return fileVersion;
    }

    public void setFileVersion(Integer fileVersion) {
        this.fileVersion = fileVersion;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public FileStatus getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(FileStatus fileStatus) {
        this.fileStatus = fileStatus;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getCreatedUserID() {
        return createdUserID;
    }

    public void setCreatedUserID(String createdUserID) {
        this.createdUserID = createdUserID;
    }

    public String getCreatedDisplayName() {
        return createdDisplayName;
    }

    public void setCreatedDisplayName(String createdDisplayName) {
        this.createdDisplayName = createdDisplayName;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public ItemAttachment getFile() {
        return file;
    }

    public void setFile(ItemAttachment file) {
        this.file = file;
    }
    
    
}
