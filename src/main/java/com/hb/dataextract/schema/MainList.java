/**
 *
 */
package com.hb.dataextract.schema;

import com.hb.dataextract.schema.projectitem.Project;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mahe
 *
 */
@XmlRootElement
public class MainList {

    private ListType list;
    private Project project;

    public ListType getList() {
        return list;
    }

    public void setList(ListType list) {
        this.list = list;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}
