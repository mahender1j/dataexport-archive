/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mahe
 */
@XmlRootElement
public class MainListBom {
    
     private ListTypeBom list;

    public ListTypeBom getList() {
        return list;
    }

    public void setList(ListTypeBom list) {
        this.list = list;
    }
     
     
     
}
