/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class WorkspaceTabListView {

    private static final long serialVersionUID = 6952855438611483090L;
    private List<WorkspaceTabListView> list;
    private String key;
    private String name;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WorkspaceTabListView> getList() {
        return list;
    }

    public void setLists(List<WorkspaceTabListView> lists) {
        this.list = lists;
    }

    @Override
    public String toString() {
        return "WorkspaceTabListView{" + "list=" + list + ", key=" + key + ", name=" + name + '}';
    }

}
