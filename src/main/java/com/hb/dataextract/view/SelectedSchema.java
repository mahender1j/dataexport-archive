/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class SelectedSchema {
    private String itemName;
    private List<SelectedItem> itemList;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public List<SelectedItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<SelectedItem> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "SelectedSchema{" + "itemName=" + itemName + ", itemList=" + itemList + '}';
    }

   
    
}
