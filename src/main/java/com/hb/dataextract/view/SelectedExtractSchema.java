/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class SelectedExtractSchema {
    private List<SelectedSchema> selectedItems;

    public List<SelectedSchema> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<SelectedSchema> selectedItems) {
        this.selectedItems = selectedItems;
    }

    @Override
    public String toString() {
        return "SelectedExtractSchema{" + "selectedItems=" + selectedItems + '}';
    }

   
    
}
