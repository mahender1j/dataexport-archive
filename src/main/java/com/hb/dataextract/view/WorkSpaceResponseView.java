/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class WorkSpaceResponseView {
    private String page;
    private List<WorkSpaceView> elements = new ArrayList<WorkSpaceView>();

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<WorkSpaceView> getElements() {
        return elements;
    }

    public void setElements(List<WorkSpaceView> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "WorkSpaceResponseView{" + "page=" + page + ", elements=" + elements + '}';
    }

    
    
    
}
