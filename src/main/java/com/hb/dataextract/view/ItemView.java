/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import java.io.Serializable;

public class ItemView implements Serializable {

    private Integer id;
    private Integer rootId;
    private Integer version;
    private String revision;
    private Integer workspaceId;
    private String url;
    private String itemDescriptor;
    private Boolean deleted;
    private Boolean isWorkingVersion;
    private Boolean isLatestVersion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(Integer workspaceId) {
        this.workspaceId = workspaceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String itemUrl) {
        this.url = itemUrl;
    }

    public String getItemDescriptor() {
        return itemDescriptor;
    }

    public void setItemDescriptor(String itemDescriptor) {
        this.itemDescriptor = itemDescriptor;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public Integer getRootId() {
        return rootId;
    }

    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public Boolean getIsWorkingVersion() {
        return isWorkingVersion;
    }

    public void setIsWorkingVersion(Boolean isWorkingVersion) {
        this.isWorkingVersion = isWorkingVersion;
    }

    public Boolean getIsLatestVersion() {
        return isLatestVersion;
    }

    public void setIsLatestVersion(Boolean isLatestVersion) {
        this.isLatestVersion = isLatestVersion;
    }

    @Override
    public String toString() {
        return "ItemView{" + "id=" + id + ", rootId=" + rootId + ", version=" + version + ", revision=" + revision + ", workspaceId=" + workspaceId + ", url=" + url + ", itemDescriptor=" + itemDescriptor + ", deleted=" + deleted + ", isWorkingVersion=" + isWorkingVersion + ", isLatestVersion=" + isLatestVersion + '}';
    }

}
