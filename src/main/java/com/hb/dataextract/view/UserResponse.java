/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class UserResponse {
    private String userid;
    private String sessionid;
    private String customerToken;
    private AuthStatus authStatus;
    private String tenantName;
    
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public AuthStatus getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(AuthStatus authStatus) {
        this.authStatus = authStatus;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }
    
    
    public String getTenantName() {
        return tenantName;
    }

    @Override
    public String toString() {
        return "UserResponse{" + "userid=" + userid + ", sessionid=" + sessionid + ", customerToken=" + customerToken + ", authStatus=" + authStatus + ", tenantName=" + tenantName + '}';
    }
    
    
    
    
}
