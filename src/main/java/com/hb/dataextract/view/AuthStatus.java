/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class AuthStatus {
    private Integer id;
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AuthStatus{" + "id=" + id + ", description=" + description + '}';
    }
    
    
}
