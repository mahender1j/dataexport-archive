/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class WorkspaceListView {
    private List<WorkSpaceView> elements;
	private WorkspaceTabListView workspaceTabList;

	public List<WorkSpaceView> getElements() {
		return elements;
	}

	public void setElements(List<WorkSpaceView> elements) {
		this.elements = elements;
	}

	public WorkspaceTabListView getWorkspaceTabList() {
		return workspaceTabList;
	}

	public void setWorkspaceTabList(WorkspaceTabListView workspaceTabList) {
		this.workspaceTabList = workspaceTabList;
	}
	
}
