/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class SelectedItem {
    private String name;
    private Boolean value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SelectedItem{" + "name=" + name + ", value=" + value + '}';
    }
    
    
    
}
