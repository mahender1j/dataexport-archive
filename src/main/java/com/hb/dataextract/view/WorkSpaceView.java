/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class WorkSpaceView {
    
      private Integer id;
      private String url;
      private String displayName;
      private String systemName;
      private String description;
      private String workspaceTypeId;
      

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkspaceTypeId() {
        return workspaceTypeId;
    }

    public void setWorkspaceTypeId(String workspaceTypeId) {
        this.workspaceTypeId = workspaceTypeId;
    }

    @Override
    public String toString() {
        return "WorkSpaceView{" + "id=" + id + ", url=" + url + ", displayName=" + displayName + ", systemName=" + systemName + ", description=" + description + ", workspaceTypeId=" + workspaceTypeId + '}';
    }

  
      
      
}
