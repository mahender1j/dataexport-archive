/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ramimmidisetti
 */
public class DataView {

    @JsonProperty("bom-item")
    private BomItemView bomItem;
    private int id;
    private String category;
    private String label;
    private String uri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public BomItemView getBomItem() {
        return bomItem;
    }

    public void setBomItem(BomItemView bomItem) {
        this.bomItem = bomItem;
    }
}
