/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.view;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Collection;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ramimmidisetti
 */
@XmlRootElement
public class PagedCollectionView<T> {

    private PageView page;
    private Collection<T> elements = null;

    public static <U> Type getType(Class<U> pagedType) {
        Type retVal = new TypeToken<PagedCollectionView<U>>() {}.getType();
        return retVal;
    }

    public PageView getPage() {
        return page;
    }

    public void setPageView(PageView page) {
        this.page = page;
    }

    public Collection<T> getElements() {
        return elements;
    }

    public void setElements(Collection<T> elements) {
        this.elements = elements;
    }
}
