/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.dao;

import com.hb.dataextract.model.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Mahe
 */
public interface SubscriberRepository extends JpaRepository<Subscriber, Long>{
    
}
