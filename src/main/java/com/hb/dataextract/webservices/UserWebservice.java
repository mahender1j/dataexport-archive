package com.hb.dataextract.webservices;

import com.hb.dataextract.exceptions.DataExtractApplicationException;
import com.hb.dataextract.exceptions.ErrorCodes;
import com.hb.dataextract.schema.MainList;
import com.hb.dataextract.schema.user.User;
import com.hb.dataextract.util.PLMUtil;
import com.hb.dataextract.view.UserResponse;
import com.hb.dataextract.view.UserView;
import com.hb.dataextract.view.WorkspaceListView;
import com.hb.dataextract.view.WorkspaceTabListView;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@CrossOrigin(origins = "http://localhost:63342")
@SuppressWarnings("deprecation")
@RestController
@RequestMapping(value = "/users")
public class UserWebservice {
    
    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserResponse retrieveUser(@RequestBody UserView userView) throws DataExtractApplicationException {
//        String autodeskURL = DataExtractConstants.DATAEXTRACT_AUTODESK_URL.replace("#", userView.getTenantName());
//        String loginUrl =  autodeskURL+"/rest/auth/1/login";
        String loginUrl = PLMUtil.getPLMLoginUrl(userView.getTenantName());
        UserResponse userResponse = null;
        System.out.println("com.hb.dataextract.webservices.UserWebservice.retrieveUser()"+userView.toString());
        if (StringUtils.isBlank(userView.getUserID()) || StringUtils.isBlank(userView.getPassword())) {
            throw new IllegalArgumentException("Missing required arguments");
        }
        ResponseEntity<UserResponse> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(loginUrl, userView, UserResponse.class);
        } catch (HttpClientErrorException hce) {
            if (hce.getMessage().contains("404")) {
                throw new DataExtractApplicationException(ErrorCodes.APPLICATION_BAD_REQUEST, hce);
            } else if (hce.getMessage().contains("400")) {
                throw new DataExtractApplicationException(ErrorCodes.SECURITY_USER_NOT_FOUND, hce);
            } else if(hce.getMessage().contains("401")){
                throw new DataExtractApplicationException(ErrorCodes.SECURITY_USER_NOT_AUTHENTICATED, hce);
            } else if(hce.getMessage().contains("423")){
                throw new DataExtractApplicationException(ErrorCodes.SECURITY_USER_ACCOUNT_LOCKED, hce);
            } else {
                throw new DataExtractApplicationException(ErrorCodes.APPLICATION_ERROR, hce);
            }
        }
        System.out.println("com.hb.dataextract.webservices.UserWebservice.retrieveUser()"+responseEntity.toString());
        userResponse = responseEntity.getBody();
        userResponse.setTenantName(userView.getTenantName());
        return userResponse;
    }
    
    @RequestMapping(value = "/userList/{tenantName}/{customerToken}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<User> getUserList(@PathVariable String tenantName, @PathVariable String customerToken, @RequestParam("sessionId") String sessionId) {
        String userListUrl = PLMUtil.getUserListUrl(tenantName);
        ResponseEntity<MainList> response = restTemplate.exchange(userListUrl, 
                HttpMethod.GET, PLMUtil.buildRequestHeaders(customerToken, sessionId), MainList.class);
        
        List<User> userList = Arrays.asList(response.getBody().getList().getUser());
        if (userList == null) return Collections.emptyList();
        return userList;
    }
}