/**
 *
 */
package com.hb.dataextract.webservices;

import com.hb.dataextract.schema.item.Detail;
import com.hb.dataextract.schema.item.ItemSelection;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.hb.dataextract.schema.MainList;
import com.hb.dataextract.util.PLMUtil;
import static com.hb.dataextract.util.PLMUtil.getSingletemsUrl;
import com.hb.dataextract.schema.ChangeLog;
import com.hb.dataextract.schema.MaterialDisposition;
import com.hb.dataextract.schema.attachment.ItemAttachment;
import com.hb.dataextract.schema.bom.BomItem;
import com.hb.dataextract.schema.projectitem.Project;
import com.hb.dataextract.schema.projectitem.ProjectItem;
import com.hb.dataextract.schema.workflow.FromState;
import com.hb.dataextract.schema.workflow.PointsList;
import com.hb.dataextract.schema.workflow.ToState;
import com.hb.dataextract.schema.workflow.Transition;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mahe
 *
 */
@CrossOrigin(origins = "http://localhost:63342")
@SuppressWarnings("deprecation")
@RestController
public class SchemaService {

    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Schema for Items Details
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
    @RequestMapping(value = "/workspace/ItemDetails/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<String> getItemSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {

        List<String> itemSchemaList = Collections.emptyList();
        itemSchemaList = getFieldName(ItemSelection.class);
        itemSchemaList.addAll(getFieldName(Detail.class));
        
        /*String itemUrl = getSingletemsUrl(tenantName, workspaceId);
        ResponseEntity<MainList> response = restTemplate.exchange(itemUrl, HttpMethod.GET, PLMUtil.buildRequestHeaders(customerToken, sessionId), MainList.class);

        List<ItemSelection> itemSelectionList = Arrays.asList(response.getBody().getList().getItem());
        if (itemSelectionList == null || itemSelectionList.isEmpty()) {
            return itemSchemaList;
        }
        for (ItemSelection item : itemSelectionList) {
            try {
                itemSchemaList = getFieldName(ItemSelection.class);
                Map<String, Object> result = PropertyUtils.describe(item);
                Detail detail = (Detail) result.get("details");
                itemSchemaList.addAll(getFieldName(Detail.class));
            } catch (Exception ex) {
                System.out.println("com.hb.dataextract.webservices.SchemaService.getItemSchema()" + ex.getMessage());
                Logger.getLogger(SchemaService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        return itemSchemaList;
    }

    /**
     * Schema for attachment
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
    @RequestMapping(value = "/workspace/Attachments/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<String> getItemAttachmentSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {
        List<String> itemAttachmentSchemaList = Collections.emptyList();
//        String attachmentUrl = PLMUtil.getAttachmentUrl(tenantName, workspaceId, dmsId);
//        ResponseEntity<MainList> response = restTemplate.exchange(attachmentUrl, HttpMethod.GET, PLMUtil.buildRequestHeaders(customerToken, sessionId), MainList.class);
//        List<ItemAttachment> listItemSelection = Arrays.asList(response.getBody().getList().getData());
//        if (listItemSelection == null || listItemSelection.isEmpty()) {
//            return itemAttachmentSchemaList;
//        }
        itemAttachmentSchemaList = getFieldName(ItemAttachment.class);
        System.out.println("com.hb.dataextract.webservices.SchemaService.getItemAttachmentSchema()"+itemAttachmentSchemaList.size()+"list"+itemAttachmentSchemaList.toString());
//        try {
//            Map<String, Object> result = PropertyUtils.describe(listItemSelection.get(0));
//            ItemAttachment file = (ItemAttachment) result.get("file");
//            itemAttachmentSchemaList.addAll(getFieldName(ItemAttachment.class));
//        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
//            e.printStackTrace();
//        }
        return itemAttachmentSchemaList;
    }
    
    /**
     * Schema for workflow
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
    @RequestMapping(value = "/workspace/WorkflowActions/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE}) 
    public Map<String, List<String>> getWorkflowSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {
        Map<String, List<String>> mapList = new HashMap<>();
        mapList.put("transition", getFieldName(Transition.class));
        mapList.put("fromState", getFieldName(FromState.class));
        mapList.put("toState", getFieldName(ToState.class));
        mapList.put("pointsList", getFieldName(PointsList.class));
        return mapList;
    }
    
    /**
     * Schema for Bill of Material (BOM)
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
  @RequestMapping(value = "/workspace/BillofMaterials/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE}) 
    public List<String> getBOMSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {  
        List<String> itemBomSchemaList = null;
        
        itemBomSchemaList = getFieldName(BomItem.class);
        return itemBomSchemaList;
    }
    
    /**
     * Schema for Change Log tab
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
    @RequestMapping(value = "/workspace/ChangeLog/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE}) 
    public List<String> getChangeLogSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {  
        List<String> changeLogSchema = null;
        
        changeLogSchema = getFieldName(ChangeLog.class);
        return changeLogSchema;
    }
    
    /**
     * schema for Material Disposition
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
    @RequestMapping(value = "/workspace/Grid/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE}) 
    public List<String> getMaterialDispositionSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {  
        List<String> materialDispositionSchema = null;
        
        materialDispositionSchema = getFieldName(MaterialDisposition.class);
        return materialDispositionSchema;
    }
    
    /**
     * This is the schema for Tasks & Deliverables tab name and key is Project Management
     * @param tenantName
     * @param customerToken
     * @param workspaceId
     * @param dmsId
     * @param sessionId
     * @return 
     */
    @RequestMapping(value = "/workspace/ProjectManagement/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE}) 
    public List<String> getTaskAndDeliverSchema(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {  
        List<String> taskAndDeliverSchema = null;        
        taskAndDeliverSchema = getFieldName(ProjectItem.class);
        return taskAndDeliverSchema;
    }

    private static List<String> getFieldName(Class<?> cls) {
        List<String> elementList = new ArrayList<>();

        Field fieldlist[] = cls.getDeclaredFields();
        for (Field field : fieldlist) {
            if (field.getType().isAssignableFrom(String.class) || 
                field.getType().isAssignableFrom(Integer.class) ||
                field.getType().isAssignableFrom(Double.class) ||
                field.getType().isAssignableFrom(Boolean.class) ||
                field.getType().isAssignableFrom(Date.class) ||
                field.getType().isAssignableFrom(Long.class)) {
                elementList.add(field.getName());
            }
        }
        return elementList;
    }

}
