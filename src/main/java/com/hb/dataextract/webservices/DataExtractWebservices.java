/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.webservices;

import com.hb.dataextract.exceptions.DataExtractApplicationException;
import com.hb.dataextract.exceptions.ErrorCodes;
import com.hb.dataextract.util.PLMUtil;
import com.hb.dataextract.view.ItemView;
import com.hb.dataextract.view.UserResponse;
import com.hb.dataextract.view.WorkSpaceResponseView;
import com.hb.dataextract.view.WorkspaceListView;
import com.hb.dataextract.view.WorkspaceTabListView;
import com.hb.dataextract.service.DataExtractService;
import com.hb.dataextract.view.ErrorResponse;
import com.hb.dataextract.view.SelectedExtractSchema;
import com.hb.dataextract.view.SelectedSchema;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "http://localhost:63342")
@SuppressWarnings("deprecation")
@RestController
public class DataExtractWebservices {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private DataExtractService dataExtractService;

    @RequestMapping(value = "/workspaces/{tenantName}/{customerToken}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public WorkSpaceResponseView retrieveWorkspaces(@PathVariable String tenantName, @PathVariable String customerToken, @RequestParam("sessionId") String sessionId) throws DataExtractApplicationException {
        //String workspacesUrl = "https://previewhummingbirdinnovations.autodeskplm360.net/api/v2/workspaces";
        
//        String autodeskURL = DataExtractConstants.DATAEXTRACT_AUTODESK_URL.replace("#", customerToken);
//        String workspacesUrl =  autodeskURL+"/api/v2/workspaces";
          System.out.println("com.hb.dataextract.webservices.DataExtractWebservices.retrieveWorkspaces()"+tenantName+" cust"+customerToken + "sess"+sessionId);
          String workspacesUrl = PLMUtil.getWorkspacesV2Url(tenantName);
          System.out.println("com.hb.dataextract.webservices.DataExtractWebservices.retrieveWorkspaces()"+workspacesUrl);
        
        if (StringUtils.isBlank(customerToken) || StringUtils.isBlank(sessionId)) {
            throw new IllegalArgumentException("Missing required arguments");
        }
        ResponseEntity<WorkSpaceResponseView> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(workspacesUrl, HttpMethod.GET, PLMUtil.buildRequestHeaders(customerToken, sessionId), WorkSpaceResponseView.class);
            System.out.println("com.hb.dataextract.webservices.DataExtractWebservices.retrieveWorkspaces()" + responseEntity.toString());
        } catch (HttpClientErrorException hce) {
            if (hce.getMessage().contains("404")) {
                throw new DataExtractApplicationException(ErrorCodes.APPLICATION_BAD_REQUEST, hce);
            } else if (hce.getMessage().contains("400")) {
                throw new DataExtractApplicationException(ErrorCodes.SECURITY_USER_NOT_FOUND, hce);
            } else if(hce.getMessage().contains("401")){
                throw new DataExtractApplicationException(ErrorCodes.SECURITY_USER_NOT_AUTHENTICATED, hce);
            } else if(hce.getMessage().contains("423")){
                throw new DataExtractApplicationException(ErrorCodes.SECURITY_USER_ACCOUNT_LOCKED, hce);
            } else {
                throw new DataExtractApplicationException(ErrorCodes.APPLICATION_ERROR, hce);
            }
        }
        return responseEntity.getBody();
    }

    @RequestMapping(value = "/workspaces/workspace/{tenantName}/{customerToken}/{workspaceId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<LinkedHashMap<String, ItemView>> getItemsList(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @RequestParam("sessionId") String sessionId) {
        //String itemsURL = "https://previewhummingbirdinnovations.autodeskplm360.net/api/v2/workspaces/" + workspaceId + "/items";
        
//        String autodeskURL = DataExtractConstants.DATAEXTRACT_AUTODESK_URL.replace("#", customerToken);
//        String itemsURL =  autodeskURL+"/api/v2/workspaces/" + workspaceId + "/items";
            
        String itemsURL = PLMUtil.geItemsV2Url(tenantName, workspaceId);
        
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(itemsURL)
                .queryParam("page", 1)
                .queryParam("page-size", 200)
                .queryParam("includeRelationships", false);
        ResponseEntity<Object> responseEntity = null;
        LinkedHashMap<String, ItemView> itemList = new LinkedHashMap<String, ItemView>();
        responseEntity = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, PLMUtil.buildRequestHeaders(customerToken, sessionId), Object.class);
        itemList = (LinkedHashMap<String, ItemView>) responseEntity.getBody();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return ResponseEntity.ok().headers(headers).body(itemList);
    }

//    @RequestMapping(value = "/workspace/items/item/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
//    public List<ItemView> getBOMList(@PathVariable Integer workspaceId, @PathVariable Integer dmsId, @PathVariable String customerToken, @RequestParam("sessionId") String sessionId) throws Exception {
//        //String bomUrl = "https://previewhummingbirdinnovations.autodeskplm360.net/api/rest/v1/workspaces/" + workspaceId + "/items/" + dmsId + "/boms";
//        
////        String autodeskURL = DataExtractConstants.DATAEXTRACT_AUTODESK_URL.replace("#", customerToken);
////        String bomUrl =  autodeskURL+"/api/rest/v1/workspaces/" + workspaceId + "/items/" + dmsId + "/boms";
//        
//
//        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(bomUrl);
//        ResponseEntity<?> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, buildRequestHeaders(customerToken, sessionId), String.class);
//        Gson gson = new Gson();
//        MainListView data = gson.fromJson(response.getBody().toString(), MainListView.class);
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
//        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
//        MainListView model = mapper.readValue(response.getBody().toString(), MainListView.class);
//        Type wkType = new TypeToken<PagedCollectionView<MainListView>>() {
//        }.getType();
//        PagedCollectionView<MainListView> items = gson.fromJson(response.getBody().toString(), wkType);
//        return Collections.emptyList();
//    }
    
    @RequestMapping(value = "/workspace/tabs/{tenantName}/{customerToken}/{workspaceId}/{dmsId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<WorkspaceTabListView> getWorkspaceTabList(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @PathVariable Integer dmsId, @RequestParam("sessionId") String sessionId) {
        //String workspaceURL = "https://previewhummingbirdinnovations.autodeskplm360.net/api/rest/v1/workspaces/" + workspaceId + "/tabs";
        String autodeskURL = String.format(PLMUtil.PLM_SERVER_FORMAT, tenantName);
        String workspaceURL =  autodeskURL+"/api/rest/v1/workspaces/" + workspaceId + "/tabs";
        
        ResponseEntity<WorkspaceListView> response = restTemplate.exchange(workspaceURL, HttpMethod.GET, PLMUtil.buildRequestHeaders(customerToken, sessionId), WorkspaceListView.class);
        return response.getBody().getWorkspaceTabList().getList();
    }
    
    @RequestMapping(value = "/workspace/extractdata/{tenantName}/{customerToken}/{workspaceId}", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public boolean extractData(@PathVariable String tenantName, @PathVariable String customerToken, @PathVariable Integer workspaceId, @RequestParam("sessionId") String sessionId,
            @RequestParam("extracttype") String extracttype,
            @RequestParam("selectedUser") String selectedUser,
            @RequestParam("startDate") String startDate,
            @RequestParam("endDate") String endDate,
            @RequestParam("recordCount") String recordCount,
            @RequestBody SelectedExtractSchema selectedExtractSchema) throws DataExtractApplicationException {
        for (SelectedSchema schema : selectedExtractSchema.getSelectedItems()) {
            System.out.println("schema" + schema.toString());
        }
        UserResponse userResponse = new UserResponse();
        userResponse.setCustomerToken(customerToken);
        userResponse.setSessionid(sessionId);
        userResponse.setTenantName(tenantName);
        List<WorkspaceTabListView> workspaceTabList = getWorkspaceTabList(tenantName, customerToken, workspaceId, null, sessionId);
        try {
            dataExtractService.extractWorkspace(workspaceId, userResponse, extracttype, workspaceTabList, selectedUser,
                    startDate, endDate, recordCount);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataExtractApplicationException(e);
        }
        return true;
    }
}
