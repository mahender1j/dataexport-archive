/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.util;

import com.hb.dataextract.exceptions.DataExtractApplicationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import static org.hibernate.criterion.Restrictions.lt;
import org.zeroturnaround.zip.commons.IOUtils;

/**
 *
 * @author Mahe
 */
public class ZipFileUtil {

    public static void zipFile(List<File> files, String extractLocation, String zipFileName) throws DataExtractApplicationException{
        if (files == null || files.isEmpty()) return;
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(extractLocation+"\\"+zipFileName+".zip"));
            for (File file: files) {                
                ZipEntry entry = new ZipEntry(file.getName());
                entry.setSize(file.length());
                entry.setTime(file.lastModified());
                out.putNextEntry(entry);
                FileInputStream in = new FileInputStream(file);
                try {
                    IOUtils.copy(in, out);
                } finally {
                    IOUtils.closeQuietly(in);
                }
                out.closeEntry();
            }
        }catch(IOException e){
            throw new DataExtractApplicationException(e);
        }
        finally {
            IOUtils.closeQuietly(out);
        }
    }
}
