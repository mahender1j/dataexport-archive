/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.util;

import com.hb.dataextract.view.UserResponse;
import java.util.Arrays;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 *
 * @author Mahe
 */
public class PLMUtil {

    public static String PLM_SERVER_FORMAT = "https://%1$s.autodeskplm360.net";

    public static String getPLMLoginUrl(String tenantName) {
        String hostname = String.format(PLM_SERVER_FORMAT, tenantName);
        return hostname + "/rest/auth/1/login";
    }

    public static String getPLMLogoutUrl(String tenantName) {
        String hostname = String.format(PLM_SERVER_FORMAT, tenantName);
        return hostname + "/rest/auth/1/logout";
    }
    
    public static String getPLMUrl(String tenantName){
        return String.format(PLM_SERVER_FORMAT, tenantName);
    }
    
    public static String getWorkspacesV2Url(String tenantName) {
        String hostname = String.format(PLM_SERVER_FORMAT, tenantName);
        return hostname + "/api/v2/workspaces";
    }

    public static String geItemsV2Url(String tenantName, Integer workspaceId) {
        return getWorkspacesV2Url(tenantName) + "/" + workspaceId + "/items";
    }

    public static String getWorkspacesUrl(String tenantName) {
        String hostname = String.format(PLM_SERVER_FORMAT, tenantName);
        return hostname + "/api/rest/v1/workspaces";
    }

    public static String geItemsUrl(String tenantName, Integer workspaceId) {
        return getWorkspacesUrl(tenantName) + "/" + workspaceId + "/items";
    }
    
    public static String getBOMUrl(String tenantName, Integer workspaceId, Integer itemId){
        return geItemsUrl(tenantName, workspaceId) + "/" + itemId + "/boms";
    }
    
    public static String getSingletemsUrl(String tenantName, Integer workspaceId) {
        return getWorkspacesUrl(tenantName) + "/" + workspaceId + "/items?page=1&size=1&includeRelationships=false";
    }

    public static String getFilesUrl(String tenantName, Integer workspaceId, Integer itemId) {
        return geItemsUrl(tenantName, workspaceId) + "/" + itemId + "/files";
    }
    
    public static String getAttachmentUrl(String tenantName, Integer workspaceId, Integer itemId) {
        return geItemsUrl(tenantName, workspaceId) + "/" + itemId + "/attachments";
    }
    
    public static String getProjectManagementUrl(String tenantName, Integer workspaceId, Integer itemId) {
        return geItemsUrl(tenantName, workspaceId) + "/" + itemId + "/projectitems";
    }
    
    public static String getDownloadAttachmentUrl(String tenantName, Integer workspaceId, Integer itemId, Integer fileID) {
        return geItemsV2Url(tenantName, workspaceId) + "/" + itemId + "/files/"+fileID;
    }
    
    public static String getWorkflowUrl(String tenantName, Integer workspaceId) {
        return getWorkspacesUrl(tenantName) + "/" + workspaceId + "/workflows/transitions";
    }
    
    public static String getUserListUrl(String tenantName){
        return getPLMUrl(tenantName)+"/api/rest/v1/users";
    }
    public static HttpHeaders getHeaderInfo(UserResponse userResponse){
        HttpHeaders headers =  new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        StringBuilder sessionInfo = new StringBuilder();
        sessionInfo.append("customer=").append(userResponse.getTenantName()).append(";").append("JSESSIONID=").append(userResponse.getSessionid());
        headers.set("Cookie", sessionInfo.toString());
        return headers;
    }
    
    public static HttpEntity buildRequestHeaders(String customerToken, String sessionId) {
        System.out.println("com.hb.dataextract.webservices.DataExtractWebservices.buildRequestHeaders()"+customerToken+sessionId);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Cookie", "customer=" + customerToken + ";JSESSIONID=" + sessionId);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        return entity;
    }
    
    public static HttpEntity buildDownloadRequestHeaders(String customerToken, String sessionId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        headers.set("Cookie", "customer=" + customerToken + ";JSESSIONID=" + sessionId);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        return entity;
    }
    
}
