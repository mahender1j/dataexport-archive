/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.batch;

import java.util.List;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Mahe
 */
public class ResponseReader<T> implements ItemReader<T>{
    private RestTemplate restTemplate = new RestTemplate();
    private final String restUrl;
    private int nextStudentIndex;
    private List<T> response;
    private Integer workspaceId;
    
    public ResponseReader(String restUrl, Integer workspaceId) {
        this.restUrl = restUrl;
        this.workspaceId = workspaceId;
    }
        
    @Override
    public T read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        //TODO: Extract REST Json and transform into excel.
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
