/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hb.dataextract.util.PLMUtil;
import com.hb.dataextract.view.ItemView;
import com.hb.dataextract.view.PagedCollectionView;
import com.hb.dataextract.view.UserResponse;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Mahe
 */
@Service
public class ItemService {
    
    private static final Integer DEFAULT_SIZE = 100;
    private final RestTemplate restTemplate = new RestTemplate();
    
    /**
     * Get the Item list with next url - next url contains page # and paqe-size attributes as query params
     * @param workspaceId
     * @param nextUrl
     * @param userResponse
     * @return 
     */
    private PagedCollectionView getItemsV2(Integer workspaceId, String nextUrl, UserResponse userResponse) {
        String itemsURL = PLMUtil.geItemsV2Url(userResponse.getTenantName(), workspaceId);
        
        UriComponentsBuilder builder = null;
        if (nextUrl == null){
            builder = UriComponentsBuilder.fromHttpUrl(itemsURL)
                .queryParam("page", 1)
                .queryParam("page-size", DEFAULT_SIZE)
                .queryParam("includeRelationships", false);
        }else{
            builder = UriComponentsBuilder.fromHttpUrl(nextUrl);
        }
        
        ResponseEntity<?> response = null;
        response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, PLMUtil.buildRequestHeaders(userResponse.getTenantName(), userResponse.getSessionid()), String.class);
        
        Gson gson = new Gson();
        Type type = new TypeToken<PagedCollectionView<ItemView>>() {
        }.getType();
        PagedCollectionView<ItemView> pagedCollectionView = gson.fromJson(response.getBody().toString(), type);
        return pagedCollectionView;
    }
    
    /**
     * Retrieve all Items Details.
     * @param workspaceId
     * @param userResponse
     * @return 
     */
    public Collection<ItemView> getItemIdList(Integer workspaceId, UserResponse userResponse){
        Collection<ItemView> itemViewList = new ArrayList<>();
        PagedCollectionView pagedCollectionView = null;
        do {
           String url = pagedCollectionView == null?null : pagedCollectionView.getPage().getNextUrl();
           pagedCollectionView = getItemsV2(workspaceId, url, userResponse);
           if (pagedCollectionView != null){
               itemViewList.addAll(pagedCollectionView.getElements());
           }
        }while (null!=pagedCollectionView.getPage() && pagedCollectionView.getPage().getNextUrl() != null);
        
        return itemViewList;
    }
}
