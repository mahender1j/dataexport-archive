/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hb.dataextract.exceptions.DataExtractApplicationException;
import com.hb.dataextract.schema.MainList;
import com.hb.dataextract.schema.MainListBom;
import com.hb.dataextract.util.PLMUtil;
import com.hb.dataextract.util.ZipFileUtil;
import com.hb.dataextract.view.ItemView;
import com.hb.dataextract.view.PagedCollectionView;
import com.hb.dataextract.view.UserResponse;
import com.hb.dataextract.view.WorkspaceTabListView;
import com.hb.dataextract.schema.attachment.ItemAttachment;
import com.hb.dataextract.schema.bom.BomItem;
import com.hb.dataextract.schema.item.CreatedByUser;
import com.hb.dataextract.schema.item.ItemSelection;
import com.hb.dataextract.schema.item.v2.ItemDetails;
import com.hb.dataextract.schema.projectitem.ProjectItem;
import static com.hb.dataextract.util.DataExtractConstants.*;
import static com.hb.dataextract.util.DateUtils.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Mahe
 */
@Service
public class DataExtractService<T> {

    private final RestTemplate restTemplate = new RestTemplate();
    private String extractlocation;
    private List<File> zipFile = new ArrayList<>();
    @Autowired
    private ItemService itemService;
    //private final Log logger = LogFactory.getLog(DataExtractService.class);
    private Integer recordCount;
    private Date startDate;
    private Date endDate;
    private String selectedUser;
    
    private RestTemplate getRestTemplateForDownload(){
        
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new ByteArrayHttpMessageConverter());
        RestTemplate restTemplateD = new RestTemplate(messageConverters);
        
        return restTemplateD;

    }
    public String extractWorkspace(Integer workspaceId, UserResponse userResponse, String extracttype, List<WorkspaceTabListView> workspaceTabList,
            String selectedUser,
            String startDate,
            String endDate,
            String recordCount) throws DataExtractApplicationException {
        this.selectedUser = selectedUser;
        this.recordCount = StringUtils.isNotEmpty(recordCount) ? Integer.valueOf(recordCount): 0;
        this.startDate = stringToDate(startDate, DDMMYYYY_FORMAT);
        this.endDate = stringToDate(endDate, DDMMYYYY_FORMAT);
        File extractFile = null;
        try {
            extractFile = File.createTempFile(workspaceId.toString()+"_workspace", ".xlsx");//TODO: provide the sftp location
            
            SXSSFWorkbook wb = new SXSSFWorkbook(SXSSFWorkbook.DEFAULT_WINDOW_SIZE);
            
            
            Collection<ItemDetails> itemList = (Collection<ItemDetails>) extractItemDetails(workspaceId, userResponse, wb);
            
            if (itemList == null || itemList.isEmpty()){
                //logger.info("No items found for workspace id :%", workspaceId);
                return null;
            }
            for (WorkspaceTabListView tabname: workspaceTabList){
                
                switch(tabname.getKey().toLowerCase()){
                    case "project management":
                          extractProjectManagement(workspaceId, userResponse, wb, itemList);
                          break;
                    case "attachments":
                          extractAttachments(workspaceId, userResponse, wb, itemList);
                          break;
                    case "workflow":
                          break;
                    case "bill of materials":
                          extractBOM(workspaceId, userResponse, wb, itemList);
                          break;
                    case "changelog":
                          break;
                    case "materialdisposition":
                          break;
                    
                    case "milestones":
                          break;
                }
                        
            }
            
            zipFile.add(extractFile);
            FileOutputStream output = new FileOutputStream(extractFile, false);
            
            wb.write(output);
            wb.close();
            wb.dispose();
            output.close();
            this.extractlocation = FilenameUtils.getFullPathNoEndSeparator(extractFile.getAbsolutePath());
            
            ZipFileUtil.zipFile(zipFile, extractlocation, FilenameUtils.getBaseName(extractFile.getAbsolutePath()));
        } catch (IOException | IllegalArgumentException | IllegalAccessException ex) {
            throw new DataExtractApplicationException(ex);
        }
        return extractFile.getAbsolutePath();
    }

    /**
     * Service metho to extract Bill of Materials
     * @param workspaceId
     * @param userResponse
     * @param bb
     * @param itemList 
     */
    public void extractBOM(Integer workspaceId, UserResponse userResponse, SXSSFWorkbook wb, Collection<ItemDetails> itemList) throws IOException, IllegalArgumentException, IllegalAccessException {
        Collection<BomItem> bomItemAllList = new ArrayList<>();
        String projMgmtUrl = null;
        for (ItemDetails item : itemList) {
            projMgmtUrl = PLMUtil.getBOMUrl(userResponse.getTenantName(), workspaceId, item.getId());

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(projMgmtUrl);

            ResponseEntity<?> responseEntity = null;
            Collection<BomItem> bomItemList = null;
            HttpEntity entity = buildRequestHeaders(userResponse.getTenantName(), userResponse.getSessionid());
            responseEntity = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, String.class);
//            if (responseEntity == null && responseEntity.getBody() == null
//                    || responseEntity.getBody().getList()== null ||
//                    responseEntity.getBody().getList().getData()== null) {
//                continue;
//            }
            ObjectMapper mapper = new ObjectMapper();
            MainListBom mainListBom = mapper.readValue(responseEntity.getBody().toString(), MainListBom.class);
            if (mainListBom == null || mainListBom.getList() == null || mainListBom.getList().getData() == null){
                continue;
            }
            bomItemList = CollectionUtils.collect(Arrays.asList(mainListBom.getList().getData()), TransformerUtils.invokerTransformer("getBomItem")) ;
//            itemAttachmentList = CollectionUtils.collect(Arrays.asList(responseEntity.getBody().getList().getData()), TransformerUtils.invokerTransformer("getFile"));
            bomItemAllList.addAll(bomItemList);
        }
        createExcel((Collection<T>) bomItemAllList, wb, "Bill of Materials");
    }
    /**
     * Extraction service for item details.
     * @param workspaceId
     * @param userResponse
     * @param wb
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException 
     */
    public Collection<T> extractItemDetails(Integer workspaceId, UserResponse userResponse, SXSSFWorkbook wb) throws IOException, IllegalArgumentException, IllegalAccessException {

        Collection<T> itemList = new ArrayList<>();
        Collection<ItemView> allItemViewList = itemService.getItemIdList(workspaceId, userResponse);

        int itemCount = 1;
        for (ItemView item : allItemViewList) {

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(item.getUrl());

            ResponseEntity<ItemDetails> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, PLMUtil.buildRequestHeaders(userResponse.getTenantName(), userResponse.getSessionid()), ItemDetails.class);

            if (response == null || response.getBody() == null) {
                continue;
            }

            ItemDetails itemDetails = response.getBody();
            
            CreatedByUser createdBy = itemDetails.getAudit().getCreatedBy();
            
            if  (createdBy == null ||  createdBy.getLoginName() == null){
                continue;
            }
            if (StringUtils.isNotEmpty(selectedUser) && !createdBy.getLoginName().equalsIgnoreCase(selectedUser)){
                continue;
            }
            
            if ((startDate != null && endDate != null) && dateBetween(itemDetails.getAudit().getCreatedOn())){
                continue;
            }
            itemList.add((T) response.getBody());
            if (itemCount == recordCount) {
                break;//When the item count reaches to max extraction count then exit the loop.
            }
            itemCount++;                       
        }
        return itemList;
    }
    
    private boolean dateBetween(String createdOn){
        if (StringUtils.isEmpty(createdOn)) return false;
        Date createdOnDate = stringToDate(createdOn, DDMMYYYY_FORMAT);
        
        if (createdOnDate == null) return false;
        if (createdOnDate.after(startDate) && createdOnDate.before(endDate)){
            return true;
        }
        
        return false;
    }
    
    /**
     * Extraction service for item attachments
     * @param workspaceId
     * @param userResponse
     * @param wb
     * @param itemList
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException 
     */
    public void extractAttachments(Integer workspaceId, UserResponse userResponse, SXSSFWorkbook wb, Collection<ItemDetails> itemList) throws IOException, IllegalArgumentException, IllegalAccessException {
        Collection<ItemAttachment> attachmentList = new ArrayList<>();
        String attachmentUrl = null;
        for (ItemDetails item : itemList) {
            attachmentUrl = PLMUtil.getAttachmentUrl(userResponse.getTenantName(), workspaceId, item.getId());

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(attachmentUrl)
                    .queryParam("page", 1)
                    .queryParam("page-size", itemList.size())
                    .queryParam("includeRelationships", false);
            ResponseEntity<MainList> responseEntity = null;
            Collection<ItemAttachment> itemAttachmentList = null;
            responseEntity = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, buildRequestHeaders(userResponse.getTenantName(), userResponse.getSessionid()), MainList.class);
            if (responseEntity != null && responseEntity.getBody() != null && responseEntity.getBody().getList() != null
                    && responseEntity.getBody().getList().getData().length > 0) {
                System.out.println("Attachment Size: " + responseEntity.getBody().getList().getData().length);
                itemAttachmentList = CollectionUtils.collect(Arrays.asList(responseEntity.getBody().getList().getData()), TransformerUtils.invokerTransformer("getFile"));
                attachmentList.addAll(itemAttachmentList);
            }

            String downloadAttachmentUrl = null;
            if (itemAttachmentList == null ) continue;
            for (ItemAttachment itemAttachment : itemAttachmentList) {
                downloadAttachmentUrl = PLMUtil.getDownloadAttachmentUrl(userResponse.getTenantName(), workspaceId, item.getId(), itemAttachment.getFileID());
                HttpEntity entity = PLMUtil.buildDownloadRequestHeaders(userResponse.getCustomerToken(), userResponse.getSessionid());

                ResponseEntity<byte[]> response = restTemplate.exchange(downloadAttachmentUrl, HttpMethod.GET, entity, byte[].class);
                File file = new File(extractlocation+"\\"+itemAttachment.getFileName());
                if (response.getStatusCode() == HttpStatus.OK) {
                    //Files.write(Paths.get(itemAttachment.getFileName()), response.getBody());
                    FileUtils.writeByteArrayToFile(file, response.getBody());
                    zipFile.add(file);
                }
            }
        }
        
        
//        Gson gson = new Gson();
//        Type wkType = new TypeToken<PagedCollectionView<ItemView>>() {
//        }.getType();
//        PagedCollectionView<T> items = gson.fromJson(responseEntity.getBody().toString(), wkType);
//        }
        createExcel((Collection<T>) attachmentList, wb, "Attachments");
    }

    
    /**
     * Extract service for Project Management / task delivery
     * @param workspaceId
     * @param userResponse
     * @param wb
     * @param itemList 
     */
   public void extractProjectManagement(Integer workspaceId, UserResponse userResponse, SXSSFWorkbook wb, Collection<ItemDetails> itemList) throws IOException, IllegalArgumentException, IllegalAccessException{
       Collection<ProjectItem> projectItemAllList = new ArrayList<>();
        String projMgmtUrl = null;
        for (ItemDetails item : itemList) {
            projMgmtUrl = PLMUtil.getProjectManagementUrl(userResponse.getTenantName(), workspaceId, item.getId());

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(projMgmtUrl);
                    
            ResponseEntity<MainList> responseEntity = null;
            Collection<ProjectItem> projectItemList = null;
            HttpEntity entity = buildRequestHeaders(userResponse.getTenantName(), userResponse.getSessionid());
            responseEntity = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,entity , MainList.class);
            if (responseEntity == null && responseEntity.getBody() == null ||
                responseEntity.getBody().getProject() == null) {
                continue;                
            }
            
            projectItemList = Arrays.asList(responseEntity.getBody().getProject().getProjectItems().getProjectItem());
            projectItemAllList.addAll(projectItemList);
        }
        createExcel((Collection<T>) projectItemAllList, wb, "Tasks Deliverables");
   }
    /**
     * Creates excel spreadsheet
     * @param itemList
     * @param wb
     * @param worksheetName
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException 
     */
    private void createExcel(Collection<T> itemList, SXSSFWorkbook wb, String worksheetName) throws IOException, IllegalArgumentException, IllegalAccessException {
        Sheet sheet = wb.getSheet(worksheetName);
        if (sheet == null){
           sheet = wb.createSheet(WorkbookUtil.createSafeSheetName(worksheetName)); 
        }
        Integer rowId = 0;
        CellStyle headerCellStyle = wb.createCellStyle();
        headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

        Font bold = wb.createFont();
        bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        bold.setFontHeightInPoints((short) 13);
        headerCellStyle.setFont(bold);
        Row row;

        Optional<T> optionalView = itemList.stream().findFirst();
        T itemView = optionalView.get();
        row = sheet.createRow(rowId++);
        int headColId = 0;

        for (Field field : itemView.getClass().getDeclaredFields()) {
            Cell cell = row.createCell(headColId++);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue((String) field.getName());
        }

        for (T iv : itemList) {
            row = sheet.createRow(rowId++);
            Integer colId = 0;
            for (Field field : iv.getClass().getDeclaredFields()) {
                Cell cell = row.createCell(colId++);

                field.setAccessible(true);
                Object objValue = field.get(iv);
                
                if (objValue == null) {
                    cell.setCellType(Cell.CELL_TYPE_BLANK);
                    cell.setCellValue(StringUtils.EMPTY);
                } else if (field.getType().isAssignableFrom(Integer.class)) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((Integer) objValue);
                }else {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    cell.setCellValue(String.valueOf(objValue));
                }
            }
        }
        
        

    }

    private HttpEntity buildRequestHeaders(String customerToken, String sessionId) {
        System.out.println("com.hb.dataextract.webservices.DataExtractWebservices.buildRequestHeaders()" + customerToken + sessionId);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Cookie", "customer=" + customerToken + ";JSESSIONID=" + sessionId);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return entity;
    }
}
