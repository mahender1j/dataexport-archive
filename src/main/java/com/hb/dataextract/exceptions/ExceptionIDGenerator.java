package com.hb.dataextract.exceptions;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class has a single static method which generates 
 * a unique id based on host name, time and single digit random ID.
 * 
 * Format: <hostname>.<yyyyMMdd>.<HHmmss>.<Random single digit>
 * Example: BEA1.20081208.230823.9
 * 
 * If system is unable to get host name, it will print HOST_UNKNOWN.
 */
public class ExceptionIDGenerator  {
    
	private static final String SEPERATOR_CHAR = ".";
	private static final String HOST_UNKNOWN = "HOST_UNKNOWN";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss");
	
	/**
	 * Returns exception ID. 
	 * @return exception ID.
	 */
	public static String getExceptionID() {
		StringBuilder sb = new StringBuilder();
		try{
			sb.append(InetAddress.getLocalHost().getHostName());  	
		} catch(UnknownHostException ue) {
			sb.append(HOST_UNKNOWN);
		}
		
		sb.append(SEPERATOR_CHAR);
		sb.append(sdf.format(new Date()));
		sb.append(SEPERATOR_CHAR);
		sb.append((int)(Math.random() * 10));
		return sb.toString(); 
	}
}
