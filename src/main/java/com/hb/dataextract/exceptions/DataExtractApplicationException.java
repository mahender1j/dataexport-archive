package com.hb.dataextract.exceptions;

import java.util.StringJoiner;

public class DataExtractApplicationException extends Exception {

	private static final long serialVersionUID = 2852860079874712265L;

	private int errorCode;
	private String errorMessage;
	private static String exceptionId;
	
	public DataExtractApplicationException(ErrorCodes errorCode) {
		super(generateMessage(errorCode.getDescription()));
		this.errorCode = errorCode.getCode();
		this.errorMessage = errorCode.getDescription();
	}
        
        public DataExtractApplicationException(Throwable throwable) {
		super(throwable);
	}

	public DataExtractApplicationException(ErrorCodes errorCode, Throwable throwable) {
		super(generateMessage(errorCode, throwable));
		this.errorCode = errorCode.getCode();
		this.errorMessage = errorCode.getDescription();
	}

	public DataExtractApplicationException() {
		super();
	}
	
	private static String generateMessage(String errorMessage) {
		String eID = ExceptionIDGenerator.getExceptionID();
		exceptionId = eID;
		StringJoiner str = new StringJoiner(", ");
		str.add("Exception ID: " + eID)
				.add("message :" + errorMessage);
		return str.toString();
	}

	private static String generateMessage(ErrorCodes errorCode, Throwable throwable) {
		String eID = ExceptionIDGenerator.getExceptionID();
		exceptionId = eID;
		StringJoiner str = new StringJoiner(", ");
		str.add("Exception ID: " + eID).add("Exception code:" + errorCode.getCode())
				.add("message :" + errorCode.getDescription()).add("Exception Class :" + throwable.getClass())
				.add("Root Cause :" + throwable.getMessage());
		return str.toString();
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getExceptionId() {
		return exceptionId;
	}

	public void setExceptionId(String exceptionId) {
		this.exceptionId = exceptionId;
	}
	
	
}
