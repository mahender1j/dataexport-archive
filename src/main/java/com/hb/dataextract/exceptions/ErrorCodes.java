package com.hb.dataextract.exceptions;

public enum ErrorCodes {
	
	  DATABASE_NOT_AVAILABLE(1000, "A database error has occured. Check your database connection."),
	  DATABASE_DUPLICATE_RECORD(1001, "No single record found. Check if table has duplicate records"),
	  DATABASE_NO_RECORD_FOUND(1002, "No record found. Check if table has a valid record."),
	  DATABASE_ERROR(1003, "Unknown database error occured."),	
	  
	  SECURITY_USER_NOT_AUTHENTICATED(2000, "User is not authenticated. Check user credentials"),
	  SECURITY_USER_TOKEN_NOT_FOUND(2002, "A token not found with the request. Check valid access token is present in the request and not expired"),
	  SECURITY_TOKEN_EXPIRED(2001, "Token has expired. Need to reauthenticate again."),
	  SECURITY_USER_NOT_FOUND(2003, "User not found. Check if username exist in User Table"),
	  SECURITY_USER_ACCOUNT_LOCKED(2004, "User Account locked."),
	  SECURITY_USER_ADMIN_ACCOUNT_LOCKED(2005, "User Account locked by Admin"),
	  SECURITY_USER_NOT_VALID_REFRESH_TOKEN(2006, "Refresh token is not valid. User has to authenticate again."),
	  SECURITY_USER_NOT_AUTHORIZED(2007, "User not authorized."),
	  SECURITY_USER_TOKEN_DECRYPT_FAILED(2008, "Security Token decryption failed, check if java cryptographic extension files exist in domain security folder."),
	  SECURITY_USER_TOKEN_ENCRYPT_FAILED(2009, "Security Token encryption failed, check if java cryptographic extension files exist in domain security folder."),
	  APPLICATION_ERROR(3001, "Application error occured."),
	  APPLICATION_ARTIVA_ACCOUNT_NOT_FOUND(3002, "Matching Account record not found."),
	  APPLICATION_BAD_REQUEST(3003, "Bad Request. Check the request"),
	  SYSTEM_UNAVAILABLE(4001, "System is unavailable. Check if the application is active and server is in running state");
	
	  private final int code;
	  private final String description;

	  private ErrorCodes(int code, String description) {
	    this.code = code;
	    this.description = description;
	  }

	  public String getDescription() {
	     return description;
	  }

	  public int getCode() {
	     return code;
	  }

	  @Override
	  public String toString() {
	    return code + ": " + description;
	  }

}
