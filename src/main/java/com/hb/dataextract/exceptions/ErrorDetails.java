package com.hb.dataextract.exceptions;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement
public class ErrorDetails {
	private String exceptionId;
	private int responseCode;
	private String detailMessage;
	
	public ErrorDetails(){	
	}
	
	public ErrorDetails(String message){
		this.detailMessage = message;
	}

	public ErrorDetails(String exceptionId, int responseCode, String detailMessage) {
		this.exceptionId = exceptionId;
		this.responseCode = responseCode;
		this.detailMessage = detailMessage;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}

	public String getExceptionId() {
		return exceptionId;
	}

	public void setExceptionId(String exceptionId) {
		this.exceptionId = exceptionId;
	}
}
