package com.hb.dataextract.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import com.hb.dataextract.exceptions.ErrorDetails;


@ControllerAdvice
public class ResponseEntityExceptionHandler
		extends org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler {
	
	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<ErrorDetails> handleUnknownException(Exception ex, WebRequest request) {			
		ErrorDetails errorDetails = new ErrorDetails("Unknown error occured");
                ex.printStackTrace();
                System.out.println("com.hb.dataextract.exceptions.ResponseEntityExceptionHandler.handleUnknownException()"+ex.getMessage());
		return new ResponseEntity<ErrorDetails> (errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
                
	}
	
	@ExceptionHandler(value = { IllegalArgumentException.class })
	protected ResponseEntity<ErrorDetails> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {			
		ErrorDetails errorDetails = new ErrorDetails("Required Parameters are missing");
		return new ResponseEntity<ErrorDetails> (errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = { DataExtractApplicationException.class })
	protected ResponseEntity<ErrorDetails> handleApplicationException(DataExtractApplicationException ex, WebRequest request) {
		return handleException(ex, request);
	}

	private ResponseEntity<ErrorDetails> handleException(DataExtractApplicationException ex, WebRequest request) {
		ErrorDetails errorDetails = null;
		errorDetails = buildErrorDetails(ex.getExceptionId(), ex.getErrorCode(), ex.getMessage());
		if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_NOT_AUTHENTICATED.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		} else if(ex.getErrorCode() == ErrorCodes.APPLICATION_ERROR.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		} else if(ex.getErrorCode() == ErrorCodes.APPLICATION_ARTIVA_ACCOUNT_NOT_FOUND.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_TOKEN_NOT_FOUND.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_ACCOUNT_LOCKED.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.LOCKED);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_NOT_FOUND.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		} else if(ex.getErrorCode() == ErrorCodes.DATABASE_NO_RECORD_FOUND.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		} else if(ex.getErrorCode() == ErrorCodes.DATABASE_DUPLICATE_RECORD.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		} else if(ex.getErrorCode() == ErrorCodes.APPLICATION_BAD_REQUEST.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_NOT_VALID_REFRESH_TOKEN.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_NOT_AUTHORIZED.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_TOKEN_ENCRYPT_FAILED.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		} else if(ex.getErrorCode() == ErrorCodes.SECURITY_USER_TOKEN_DECRYPT_FAILED.getCode()){
			return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}
		errorDetails = new ErrorDetails("Unknown error occured");
		return new ResponseEntity<ErrorDetails> (errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private ErrorDetails buildErrorDetails(String exceptionId, int responseCode, String detailMessage){
		return new ErrorDetails(exceptionId, responseCode, detailMessage);
	}
}
