/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hb.dataextract.schema;

import com.hb.dataextract.exceptions.DataExtractApplicationException;
import com.hb.dataextract.view.UserResponse;
import com.hb.dataextract.view.UserView;
import com.hb.dataextract.webservices.SchemaService;
import com.hb.dataextract.webservices.UserWebservice;
import com.hb.dataextract.service.DataExtractService;
import com.hb.dataextract.view.WorkspaceTabListView;
import com.hb.dataextract.webservices.DataExtractWebservices;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mahe
 */
@Ignore
public class SchemaTest {
    private UserView userView;
    private UserWebservice userWebservice;
    private SchemaService schemaService;
    private UserResponse userResponse;
         
    public SchemaTest() {
    }
    
    private DataExtractWebservices dataExtractWebservices = new DataExtractWebservices();
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws DataExtractApplicationException {
        userView = new UserView();
        userView.setTenantName("previewhummingbirdinnovations");
        userView.setUserID("mahender@hummingbirdinnovations.com");
        userView.setPassword("hummingbird123");
        userWebservice = new UserWebservice();
        schemaService = new SchemaService();
        userResponse = userWebservice.retrieveUser(userView);
    }
    
    @After
    public void tearDown() {
    }

     @Test
     public void hello() {
         System.out.println("Test");
     }
     
      @Test
     public void testItemSchems() throws DataExtractApplicationException{
        
         List<String> itemSchemList = schemaService.getItemSchema(userResponse.getTenantName(), userResponse.getCustomerToken(), 57, 0, userResponse.getSessionid());
         
         System.out.println(itemSchemList);
     }
     
     @Test
     public void testItemAttachmentSchema() throws DataExtractApplicationException{
         List<String> itemAttachmentSchema = schemaService.getItemAttachmentSchema(userResponse.getTenantName(), userResponse.getCustomerToken(), 57, 6918, userResponse.getSessionid());
         System.out.println(itemAttachmentSchema);
     }
     
     @Test
     public void testItemWorkflowSchema() throws DataExtractApplicationException{
         Map<String, List<String>> itemWorkflowSchema = schemaService.getWorkflowSchema(userResponse.getTenantName(), userResponse.getCustomerToken(), 9, 7296, userResponse.getSessionid());
         System.out.println("Size: " + itemWorkflowSchema.size());
         System.out.println(itemWorkflowSchema);
     }
     
     @Test
     public void testExcelExtract() throws DataExtractApplicationException, IllegalAccessException, IOException{
         DataExtractService dataExtractService = new DataExtractService();
         String extractType = "excel";
         List<WorkspaceTabListView> workspaceTabList = dataExtractWebservices.getWorkspaceTabList(userResponse.getTenantName(), userResponse.getCustomerToken(), 26, null, userResponse.getSessionid());
         //dataExtractService.extractWorkspace(57, userResponse, extractType, workspaceTabList);
     }
     
    @Test
     public void testBOMSchema() throws DataExtractApplicationException, IllegalAccessException, IOException{
         List<String> bmschema = schemaService.getBOMSchema(userResponse.getTenantName(), userResponse.getCustomerToken(), 57, 6976, userResponse.getSessionid());
         System.out.println(bmschema);
     }
}
